﻿using System.Collections;
using System.Collections.Generic;
using stats;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class StatsResetScript : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(() =>
        {
            StatsManager.reset();

            foreach (var statsValueTextScript in FindObjectsOfType<StatsValueTextScript>())
            {
                statsValueTextScript.UpdateStats();
            }

            ProgressionManager.init();
            foreach (var script in FindObjectsOfType<LevelMapButtonScript>())
            {
                script.InitStars();
            }
        });
    }

    // Update is called once per frame
    void Update()
    {
    }
}