﻿using System.Linq;
using UniRx;
using UnityEngine;
using Utils;

public class FlowerScript : MonoBehaviour, TurnManager.ITurnStartActionTaker,
    TurnManager.ITurnEndActionTaker, UndoComponent.IUndoStateCreator

{
    private GameObject player;
    [Range(1, 10)] public int visibilityRange;

    public float[] raycastLevels;

    private bool isAttackedInThisRound;

    public Vector3 fireOriginOffset;

    [Range(0, 10)] public float fireSpeed = 1;

    private IObservable<GameObject> turnEndObservable;


    internal class UndoState : UndoComponent.IUndoState
    {
        internal readonly Vector3 position;
        internal readonly Quaternion rotation;
        internal readonly bool isAlive;

        public UndoState(Vector3 position, Quaternion rotation,bool isAlive)
        {
            this.position = position;
            this.rotation = rotation;
            this.isAlive = isAlive;
        }
    }

    private void Awake()
    {
        player = FindObjectOfType<PlayerComponent>().gameObject;
    }

    // Use this for initialization
    void Start()
    {
        SetJumperComponentEnabled(false);

        //Notify enemy will participate to turn action takers
        FindObjectOfType<TurnManager>().AddTurnStartActionTaker(this);
        FindObjectOfType<TurnManager>().AddTurnEndActionTaker(this);
    }
    
    void OnDisable()
    {
        SetJumperComponentEnabled(true);
    }

    void OnEnable()
    {
        SetJumperComponentEnabled(false);
    }

    private void SetJumperComponentEnabled(bool isEnabled)
    {
        var jumpPointComp = FrogUtils.FindGameObjectsAtPosition(transform.position)
            .ToList().Select(go => go.GetComponent<JumpPointComponent>())
            .FirstOrDefault(component => component != null);

        if (jumpPointComp != null) jumpPointComp.enabled = isEnabled;
    }

    private void OnTriggerEnter(Collider other)
    {
        //this own flower projectile will not destroy the flower
        if (isAttackedInThisRound) return;

        if (other.GetComponent<ProjectileComponent>() != null)
        {
            other.GetComponent<ProjectileComponent>().StopAttack();
            KillFlower();
        }
    }

    private void KillFlower()
    {
        gameObject.active = false;
//        TurnManager.INSTANCE.RemoveTurnEndActionTaker(this);
//        TurnManager.INSTANCE.RemoveTurnStartActionTaker(this);
//        Destroy(gameObject);
    }

    private void AttackTarget(GameObject go)
    {
        isAttackedInThisRound = true;
//        var projectile = FrogUtils.InstantiatePrefab("ProjectilePrefab");
        var projectile = FrogUtils.InstantiatePrefab("ProjectilePrefab");
        var fireOrigin = new Vector3(transform.position.x + fireOriginOffset.x,
            transform.position.y + fireOriginOffset.y,
            transform.position.z + fireOriginOffset.z);
        projectile.transform.position = fireOrigin;
        projectile.transform.forward =
            FrogUtils.RotateTowardsDestination(projectile.transform.position, go.transform.position);

        turnEndObservable = projectile.GetComponent<ProjectileComponent>()
            .InitProjectile(fireOrigin, go.transform.position, fireSpeed);

        //play the attack moveAnimation
        GetComponent<Animator>().SetTrigger("Attack");
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        var pos = new Vector3(transform.position.x,
            transform.position.y,
            transform.position.z);

        foreach (var level in raycastLevels)
        {
            pos.y = transform.position.y + level;
            Gizmos.DrawRay(pos, transform.forward * visibilityRange);
        }

        //draw fire origin
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(new Vector3(transform.position.x + fireOriginOffset.x,
            transform.position.y + fireOriginOffset.y,
            transform.position.z + fireOriginOffset.z), 0.08f);
    }


    public void TakeTurnStartAction()
    {
        if (!isActiveAndEnabled) return;
        TryToAttack();
        isAttackedInThisRound = false;
    }

    private void TryToAttack()
    {
        if (!isActiveAndEnabled) return;

        //flower will not fire while he is dragged by other platform
        if (GetComponent<DraggedObjectComp>() != null && GetComponent<DraggedObjectComp>().isActiveAndEnabled) return;
        if (isAttackedInThisRound) return;

        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        var from = new Vector3(transform.position.x,
            transform.position.y,
            transform.position.z);

        foreach (var level in raycastLevels)
        {
            from.y = transform.position.y + level;
            RaycastHit[] hits;
            hits = Physics.RaycastAll(from, fwd, visibilityRange);

            for (int i = 0; i < hits.Length; i++)
            {
                RaycastHit hit = hits[i];
                if (hit.distance > visibilityRange) continue;
                if (hit.collider.gameObject.tag.Equals("Player") || hit.collider.gameObject.tag.Equals("Enemy"))
                {
                    AttackTarget(hit.collider.gameObject);
                    return;
                }
            }
        }
    }

    public IObservable<GameObject> TakeTurnEndsAction()
    {
        if (!isActiveAndEnabled) return Observable.Empty<GameObject>();
        TryToAttack();
        var retObs = turnEndObservable;
        turnEndObservable = null;
        return (retObs != null) ? retObs : Observable.Empty<GameObject>();
    }

    public int GetTurnEndPriority()
    {
        return 6;
    }

    public UndoComponent.IUndoState createUndoState()
    {
        return new UndoState(FrogUtils.CloneVector3(transform.position),
            FrogUtils.CloneQuaternion(transform.rotation),gameObject.active);
    }


    public void undoState(UndoComponent.IUndoState state)
    {
        var undoState = state as UndoState;
        transform.position = undoState.position;
        transform.rotation = undoState.rotation;
        gameObject.active = undoState.isAlive;
    }
    
    public void TurnWillEnd()
    {
    }
}