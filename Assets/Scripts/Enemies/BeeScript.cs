﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using Utils;

[RequireComponent(typeof(JumperComponent))]
public class BeeScript : MonoBehaviour, TurnManager.ITurnEndActionTaker, JumperComponent.IJumpListener
    , UndoComponent.IUndoStateCreator
{
    private JumperComponent mJumperComp;
    private IObserver<GameObject> jumpObserver;
    private IObservable<GameObject> jumpObservable;
    public float[] raycastLevels;
    [Range(1, 10)] public int visibilityRange;
    private Vector3 direction;
    private Vector3 initialPosition;

    internal class UndoState : UndoComponent.IUndoState
    {
        internal readonly Vector3 position;
        internal readonly Quaternion rotation;
        internal readonly Vector3 direction;
        internal readonly bool isAlive;

        public UndoState(Vector3 position, Quaternion rotation,Vector3 direction,bool isAlive)
        {
            this.position = position;
            this.rotation = rotation;
            this.isAlive = isAlive;
            this.direction = direction;
        }
    }

    // Use this for initialization
    void Start()
    {
        mJumperComp = GetComponent<JumperComponent>();
        initialPosition = FrogUtils.CloneVector3(transform.position);
        direction = transform.forward;
//        TurnManager.INSTANCE.AddTurnEndActionTaker(this);
        FindObjectOfType<TurnManager>().AddTurnEndActionTaker(this);
    }

    // Update is called once per frame
    void Update()
    {
    }

    public IObservable<GameObject> TakeTurnEndsAction()
    {
        if (!isActiveAndEnabled) return Observable.Empty<GameObject>();
        //Debug.Log("Bee takes Turn End Action");

        Vector3 jumpDestination = FindJumpDestination();

        //if there is nowhere to jump,just don't jump
        if (jumpDestination == Vector3.zero) return Observable.Empty<GameObject>();

        GetComponent<JumperComponent>().RotateTowardsDestination(jumpDestination);
        GetComponent<JumperComponent>().StartJump(jumpDestination, this);

        return jumpObservable;
    }

    public int GetTurnEndPriority()
    {
        return 7;
    }

    private Vector3 FindJumpDestination()
    {
        var nextPosition = transform.position + direction * 2;

        float distanceFromOrigin = Vector3.Distance(nextPosition, initialPosition);
        bool shouldTurnAround = distanceFromOrigin > visibilityRange;

        //jump point that we are standing on
        var nextJumpPointComp = NextJumpPointComp(nextPosition);

        if (nextJumpPointComp == null || shouldTurnAround)
        {
            direction = transform.forward * -1;
            nextPosition = transform.position + direction * 2;
            nextJumpPointComp = NextJumpPointComp(nextPosition);
        }

        if (nextJumpPointComp == null) return Vector3.zero;

        return nextJumpPointComp.getJumpTouchPoint();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ProjectileComponent>() != null)
        {
            other.GetComponent<ProjectileComponent>().StopAttack();
            KillBee();
        }

//        if (other.gameObject.tag.Equals("Enemy"))
//        {
//            //TODO : Need to notify game manager...
//            print("Bee was killed by an Enemy");
//            KillBee();
//        }
    }

    private void KillBee()
    {
        if (jumpObserver != null)
        {
            jumpObserver.OnNext(gameObject);
            jumpObserver.OnCompleted();
            jumpObserver = null;
        }

        gameObject.active = false;
//        TurnManager.INSTANCE.RemoveTurnEndActionTaker(this);
//        Destroy(gameObject);
    }


    private static JumpPointComponent NextJumpPointComp(Vector3 nextPosition)
    {
        return FrogUtils.FindGameObjectsAtPosition(nextPosition, 1f)
            .Where(o => o.GetComponent<JumpPointComponent>() != null)
            .Select(o => o.GetComponent<JumpPointComponent>())
            .FirstOrDefault();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        var pos = new Vector3(transform.position.x,
            transform.position.y,
            transform.position.z);

        foreach (var level in raycastLevels)
        {
            pos.y = level;
            //forward
            Gizmos.DrawRay(pos, transform.forward * visibilityRange);
            //backward
            Gizmos.DrawRay(pos, transform.forward * -1 * visibilityRange);
        }
    }

    private IObservable<GameObject> CreateJumpObservable()
    {
        return Observable.Create<GameObject>(observer =>
        {
            jumpObserver = observer;
            return Disposable.Empty;
        });
    }

    public void OnJumpStart()
    {
        jumpObservable = CreateJumpObservable();
    }

    public void OnJumpEnd()
    {
        jumpObserver.OnNext(gameObject);
        jumpObserver.OnCompleted();
        jumpObserver = null;
    }

    public UndoComponent.IUndoState createUndoState()
    {
        return new UndoState(FrogUtils.CloneVector3(transform.position),
            FrogUtils.CloneQuaternion(transform.rotation),direction,gameObject.active);
    }


    public void undoState(UndoComponent.IUndoState state)
    {
        var undoState = state as UndoState;
        transform.position = undoState.position;
        transform.rotation = undoState.rotation;
        gameObject.active = undoState.isAlive;
        direction = undoState.direction;
    }
    
    public void TurnWillEnd()
    {
    }
}