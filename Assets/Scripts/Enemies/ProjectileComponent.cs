﻿using UniRx;
using UnityEngine;

public class ProjectileComponent : MonoBehaviour/*, TurnManager.ITurnEndActionTaker*/
{
    public Vector3 destination;
    public Vector3 fireOrigin;
    private float projectileDestinationPrecentage;
    public float fireSpeed;

    private IObserver<GameObject> turnEndObserver;
    private IObservable<GameObject> projectileObservable;

    // Use this for initialization
    void Start()
    {
//        TurnManager.INSTANCE.AddTurnEndActionTaker(this);
    }

    // Update is called once per frame
    void Update()
    {
        projectileDestinationPrecentage += Time.deltaTime * fireSpeed;
        transform.position = Vector3.Lerp(fireOrigin,
            destination, projectileDestinationPrecentage);

        if (projectileDestinationPrecentage > 1.1) StopAttack();
    }

    public void StopAttack()
    {
//        TurnManager.INSTANCE.RemoveTurnEndActionTaker(this);
        projectileDestinationPrecentage = 0;
        Destroy(gameObject);
        if (turnEndObserver != null)
        {
            turnEndObserver.OnNext(gameObject);
            turnEndObserver.OnCompleted();
            turnEndObserver = null;
            projectileObservable = null;
        }
    }

    public IObservable<GameObject> InitProjectile(Vector3 origin, Vector3 destination, float speed)
    {
        fireOrigin = origin;
        this.destination = destination;
        fireSpeed = speed;
        
        projectileObservable = CreateObservable();
        return projectileObservable;
    }

    private IObservable<GameObject> CreateObservable()
    {
        return Observable.Create<GameObject>(observer =>
        {
            turnEndObserver = observer;
            return Disposable.Empty;
        });
    }
}