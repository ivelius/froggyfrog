﻿using System;
using UniRx;
using UnityEngine;
using Utils;

[RequireComponent(typeof(PathFinderComp))]
[RequireComponent(typeof(JumperComponent))]
public class CrocodileComp : MonoBehaviour, TurnManager.ITurnEndActionTaker,
    JumperComponent.IJumpListener,UndoComponent.IUndoStateCreator
{
    private Vector3 jumpDestination = Vector3.zero;
    public float[] raycastLevels;
    [Range(1, 10)] public int visibilityRange;
    private GameObject player;

    private Vector3 lastSeenPlayerPosition;
    private Vector3 initialPos;
    private UniRx.IObserver<GameObject> jumpObserver;
    private UniRx.IObservable<GameObject> jumpObservable;
    
    internal class UndoState : UndoComponent.IUndoState
    {
        internal readonly Vector3 position;
        internal readonly Quaternion rotation;
        internal readonly Vector3 lastSeenPlayerPosition;
        internal readonly bool isAlive;

        public UndoState(Vector3 position, Quaternion rotation,Vector3 lastSeenPlayerPosition,bool isAlive)
        {
            this.position = position;
            this.rotation = rotation;
            this.isAlive = isAlive;
            this.lastSeenPlayerPosition = lastSeenPlayerPosition;
        }
    }

    private void Awake()
    {
        player = FindObjectOfType<PlayerComponent>().gameObject;
        lastSeenPlayerPosition = FrogUtils.CloneVector3(transform.position);
        initialPos = FrogUtils.CloneVector3(transform.position);
    }

    // Use this for initialization
    void Start()
    {
        //Notify enemy will participate to turn end action takers
        FindObjectOfType<TurnManager>().AddTurnEndActionTaker(this);
//        TurnManager.INSTANCE.AddTurnEndActionTaker(this);
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < -1) KillCrockodile();
    }

    private void KillCrockodile()
    {
        if (jumpObserver != null)
        {
            jumpObserver.OnNext(gameObject);
            jumpObserver.OnCompleted();
            jumpObserver = null;
        }

        gameObject.active = false;
//        TurnManager.INSTANCE.RemoveTurnEndActionTaker(this);
//        Destroy(gameObject);
    }

    void FixedUpdate()
    {
        if (!isActiveAndEnabled) return;

        var distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);
        if (distanceToPlayer <= visibilityRange)
            lastSeenPlayerPosition = FrogUtils.CloneVector3(player.transform.position);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        var pos = new Vector3(transform.position.x,
            transform.position.y,
            transform.position.z);

        foreach (var level in raycastLevels)
        {
            pos.y = level;
            //forward
            Gizmos.DrawRay(pos, transform.forward * visibilityRange);
            //backward
            Gizmos.DrawRay(pos, transform.forward * -1 * visibilityRange);
            //right
            Gizmos.DrawRay(pos, transform.right * visibilityRange);
            //left
            Gizmos.DrawRay(pos, transform.right * -1 * visibilityRange);
        }
    }

    public UniRx.IObservable<GameObject> TakeTurnEndsAction()
    {
        if (!isActiveAndEnabled) return Observable.Empty<GameObject>();
        
        if (FrogUtils.IsZroDistance(initialPos, lastSeenPlayerPosition))
        {
            //When destination is reached , crockodile will return back to his initial position
            lastSeenPlayerPosition = initialPos;
            return Observable.Empty<GameObject>();
        }
        jumpDestination = FindJumpDestination();

        //if there is nowhere to jump,just don't jump
        if (jumpDestination == Vector3.zero) return Observable.Empty<GameObject>();

        GetComponent<JumperComponent>().RotateTowardsDestination(jumpDestination);
        GetComponent<JumperComponent>().StartJump(jumpDestination, this);
        GetComponent<Animator>().SetTrigger("Jump");
        return jumpObservable;
    }

    public int GetTurnEndPriority()
    {
        return 8;
    }

    public void TurnWillEnd()
    {
    }

    private UniRx.IObservable<GameObject> CreateJumpObservable()
    {
        return Observable.Create<GameObject>(observer =>
        {
            jumpObserver = observer;
            return Disposable.Empty;
        });
    }


    private Vector3 FindJumpDestination()
    {
        var allJumpableObjects = GetComponent<PathFinderComp>().FindReachableJumpPoints();
        if (allJumpableObjects.Length == 0) return Vector3.zero;


        var destination = allJumpableObjects[0].GetComponent<JumpPointComponent>().getJumpTouchPoint();
        var currDistance = Vector3.Distance(transform.position, lastSeenPlayerPosition);

        foreach (var jumpableObj in allJumpableObjects)
        {
            var newDestination = jumpableObj.GetComponent<JumpPointComponent>().getJumpTouchPoint();
            var newDistance = Vector3.Distance(
                newDestination,
                lastSeenPlayerPosition);
            if (newDistance < currDistance)
            {
                currDistance = newDistance;
                destination = newDestination;
            }
        }

        return destination;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ProjectileComponent>() != null)
        {
            other.GetComponent<ProjectileComponent>().StopAttack();
            KillCrockodile();
        }
        
        if (other.gameObject.tag.Equals("Enemy"))
        {
            //TODO : Need to notify game manager...
            print("Crockodile was killed by an Enemy");
            KillCrockodile();
        }
    }

    public void OnJumpStart()
    {
        jumpObservable = CreateJumpObservable();
    }

    public void OnJumpEnd()
    {
        jumpObserver.OnNext(gameObject);
        jumpObserver.OnCompleted();
    }
    
    public UndoComponent.IUndoState createUndoState()
    {
        return new UndoState(FrogUtils.CloneVector3(transform.position),
            FrogUtils.CloneQuaternion(transform.rotation),lastSeenPlayerPosition,gameObject.active);
    }


    public void undoState(UndoComponent.IUndoState state)
    {
        var undoState = state as UndoState;
        transform.position = undoState.position;
        transform.rotation = undoState.rotation;
        gameObject.active = undoState.isAlive;
        lastSeenPlayerPosition = undoState.lastSeenPlayerPosition;
    }
}