﻿namespace Utils.Player
{
    public interface IPlayerState
    {
        void OnStateActivated();
        void OnStateDeactivated();
        void OnUpdate();
        void onTakeTurnStartAction();
    }
}