﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Runtime.InteropServices;

namespace Utils.Player
{
    public class PlayerDefaultState : IPlayerState, JumperComponent.IJumpListener
    {
        private readonly PlayerComponent playerComponent;
        private JumperComponent jumerComp;
        private PathFinderComp pathFinderComp;
        private Animator animatorComp;
        private bool isMovedThisTurn;


        public PlayerDefaultState(PlayerComponent playerComponent)
        {
            this.playerComponent = playerComponent;
        }

        public void OnStateActivated()
        {
            //Does nothing
        }

        public void OnStateDeactivated()
        {
            //Does Nothing
        }

        public void OnUpdate()
        {
            if (isMovedThisTurn) return;

            if (Input.GetMouseButtonUp(0))
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit[] hits;
                hits = Physics.RaycastAll(ray);

                foreach (var hit in hits)
                {
                    var touchPoint = hit.collider.GetComponent<JumpPointComponent>();
                    if (touchPoint == null) continue;

                    //we limit the jump according to path finder component
                    if (!IsReachable(touchPoint)) return;

                    LazyLoadJumperComponent();
                    jumerComp.RotateTowardsDestination(touchPoint.getJumpTouchPoint());
                    jumerComp.StartJump(touchPoint.getJumpTouchPoint(), this);
                    isMovedThisTurn = true;
                    return;
                }
            }
        }

        public void onTakeTurnStartAction()
        {
            //Debug.Log("Player Takes Turn start action");
            isMovedThisTurn = false;
        }




        private bool IsReachable(JumpPointComponent jmpPointComp)
        {
            LazyLoadPathFinderComponent();
            return pathFinderComp.FindReachableJumpPoints().Contains(jmpPointComp);
        }

        private void LazyLoadPathFinderComponent()
        {
            if (pathFinderComp == null) pathFinderComp = playerComponent.GetComponent<PathFinderComp>();
        }

        private void LazyLoadJumperComponent()
        {
            if (jumerComp == null) jumerComp = playerComponent.GetComponent<JumperComponent>();
        }

        private void LazyLoadAnimatorComponent()
        {
            if (animatorComp == null) animatorComp = playerComponent.GetComponent<Animator>();
        }

        public void OnJumpStart()
        {
//            TurnManager.INSTANCE.OnPlayerWillMakeMove();
            Object.FindObjectOfType<TurnManager>().OnPlayerWillMakeMove();
            
            LazyLoadAnimatorComponent();
            //Play moveAnimation
            animatorComp.SetTrigger("Jump");
        }

        public void OnJumpEnd()
        {
            //Notify that player finished his turn
//            TurnManager.INSTANCE.OnPlayerFinishedTurn(playerComponent.gameObject);
            Object.FindObjectOfType<TurnManager>().OnPlayerFinishedTurn(playerComponent.gameObject);
        }

    }
}