﻿
using Utils.Items;

namespace Utils.Player
{
    public class PlayerBoosterUsageState : IPlayerState
    {
        private PlayerComponent playerComponent;
        private IBooster booster;

        public PlayerBoosterUsageState(PlayerComponent playerComponent, IBooster booster)
        {
            this.playerComponent = playerComponent;
            this.booster = booster;
        }

        public void OnStateActivated()
        {
            booster.AddBoosterComponent(playerComponent.gameObject);
        }

        public void OnStateDeactivated()
        {
            booster.RemoveBoosterComponent(playerComponent.gameObject);
        }

        public void OnUpdate()
        {
            //TODO
        }

        public void onTakeTurnStartAction()
        {
        }
    }
}