﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using stats;
using UniRx;
using UnityEngine;
using Utils;
using Utils.Items;
using Utils.Items.Collectables;
using Utils.Player;

[RequireComponent(typeof(PathFinderComp))]
[RequireComponent(typeof(JumperComponent))]
public class PlayerComponent : MonoBehaviour, IItemsCollector, TurnManager.ITurnStartActionTaker,
    UndoComponent.IUndoStateCreator
{
    public const string PLAYER_DEAD_EVENT = "OnPlayerDied";
    public const string PLAYER_WIN_EVENT = "OnPlayerWin";

    private IPlayerState playerState;
//    private BackPackComponent backpackComponent;


    internal class UndoState : UndoComponent.IUndoState
    {
        internal readonly Vector3 position;
        internal readonly Quaternion rotation;

        public UndoState(Vector3 position, Quaternion rotation)
        {
            this.position = position;
            this.rotation = rotation;
        }
    }

    private void Awake()
    {
//        backpackComponent = FindObjectOfType<BackPackComponent>().GetComponent<BackPackComponent>();
        DefaultState();
    }


    // Use this for initialization
    void Start()
    {
        //Notify enemy will participate to turn end action takers
//        TurnManager.INSTANCE.AddTurnStartActionTaker(this);
        FindObjectOfType<TurnManager>().AddTurnStartActionTaker(this);
    }

    public void TakeTurnStartAction()
    {
        playerState.onTakeTurnStartAction();
//        GetComponent<PathFinderComp>().HighlightReachablePlatforms();
    }

    // Update is called once per frame
    void Update()
    {
        //Check game over conditions
        if (transform.position.y < -1)
        {
            KillPlayer("Player has drowned");
            return;
        }

        playerState.OnUpdate();
    }

    private void KillPlayer(string msg)
    {
        Destroy(gameObject);
        NotificationCenter.DefaultCenter.PostNotification(this,
            PLAYER_DEAD_EVENT);
        
        //TODO : If there is no lives , level should not start !
        var lives = StatsManager.GetIntStatFromPreferences(StatsManager.Type.LIVES);
        StatsManager.StoreIntStatInPreferences(StatsManager.Type.LIVES, lives - 1);  
    }

    public void UseBooster(IBooster booster)
    {
        ChangePlayerState(new PlayerBoosterUsageState(this, booster));
    }

    public void DefaultState()
    {
        ChangePlayerState(new PlayerDefaultState(this));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ProjectileComponent>() != null)
        {
            KillPlayer("Player was hit by a projectile");
        }

        if (other.gameObject.tag.Equals("Enemy"))
        {
            //TODO : Need to notify game manager...
            KillPlayer("Player was killed by an Enemy");
        }
    }

//    private bool IsReachable(JumpPointComponent jmpPointComp)
//    {
//        return GetComponent<PathFinderComp>().FindReachableJumpPoints().Contains(jmpPointComp);
//    }


    public void onItemCollected(ICollectableItem item)
    {
        if (item is Screw)
        {
            NotificationCenter.DefaultCenter.PostNotification(this,
                PLAYER_WIN_EVENT);
        }

        if (item is Butterfly)
        {
            var script = FindObjectOfType<ButterfliesUiScript>();
            if (script != null) script.addButterFlyAsCollected();
            var newBtfAmount = StatsManager.GetIntStatFromPreferences(StatsManager.Type.BUTTERFLIES) + 1;
            StatsManager.StoreIntStatInPreferences(StatsManager.Type.BUTTERFLIES,newBtfAmount);
        }
    }

    public void OnItemUsed(StoneBooster stoneBoosterItem)
    {
        ChangePlayerState(
            new PlayerDefaultState(this));
    }

    private void ChangePlayerState(IPlayerState state)
    {
        if (playerState != null) playerState.OnStateDeactivated();
        playerState = state;
        playerState.OnStateActivated();
    }

    public UndoComponent.IUndoState createUndoState()
    {
        return new UndoState(FrogUtils.CloneVector3(transform.position),
            FrogUtils.CloneQuaternion(transform.rotation));
    }


    public void undoState(UndoComponent.IUndoState state)
    {
        var undoState = state as UndoState;
        transform.position = undoState.position;
        transform.rotation = undoState.rotation;
    }
}