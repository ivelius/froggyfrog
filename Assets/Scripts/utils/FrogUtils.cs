﻿using System;
using System.Linq;
using UnityEngine;

namespace Utils
{
    public class FrogUtils
    {
        public static GameObject[] FindGameObjectsAtPosition(Vector3 pos, float radius = 0.3f)
        {
            var collidedGameObjects =
                Physics.OverlapSphere(pos, radius /*Radius*/)
                    .Select(c => c.gameObject)
                    .Where(o => !o.tag.Equals("MapEditor"))
                    .ToArray();
            return collidedGameObjects;
        }

        public static GameObject InstantiatePrefab(string prefabName)
        {
            var prefab = (GameObject) Resources.Load("Prefabs/" + prefabName);
            return GameObject.Instantiate(prefab);
        }

        public static bool IsZroDistance(Vector3 a, Vector3 b)
        {
            var distance = Vector3.Distance(a, b);
            return distance == 0 || distance < 0.03;
        }

        public static Vector3 CloneVector3(Vector3 other)
        {
            return new Vector3(other.x, other.y, other.z);
        }

        /// <summary>
        /// Returns the "Forward" vector
        /// </summary>
        /// <param name="position"></param>
        /// <param name="destination"></param>
        /// <returns></returns>
        public static Vector3 RotateTowardsDestination(Vector3 position, Vector3 destination)
        {
            return Vector3.Normalize(destination - position);
        }

        public static Quaternion CloneQuaternion(Quaternion quaternion)
        {
            return new Quaternion(quaternion.x, quaternion.y, quaternion.z, quaternion.w);
        }
    }
}