﻿using System;
using Newtonsoft.Json;

namespace utils
{
    internal class JsonSerializationUtils
    {
        public static T DeserializeFromJsonString<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static string SerializeToJsonString(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}