﻿using System;
using UnityEngine;

namespace utils
{
    /// <summary>
    /// Encapsulates the moveAnimation of the column trigger
    /// </summary>
    public class RotateAnimation
    {
        private float angularSpeed;
        private float direction;
        private Predicate<float> endCondition;
        private Action endAction;
        private Action updateAction;
        private Transform parentTransform;
        private Quaternion destination;
        public bool IsFinishedAnimating { get; set; }

        public RotateAnimation(Transform parentTransform, float angularSpeed,
            Quaternion destination, Action endAction,
            Action updateAction = null)
        {
            this.parentTransform = parentTransform;
            this.angularSpeed = angularSpeed;
            this.destination = destination;
            this.endAction = endAction;
            this.updateAction = updateAction;
        }


        /// <summary>
        /// Update must be called every frame in order to animate
        /// </summary>
        public void Update()
        {
            if (IsFinishedAnimating) return;
            var step = angularSpeed * Time.deltaTime;
            parentTransform.rotation = Quaternion.RotateTowards(parentTransform.rotation, destination, step);
            if (parentTransform.rotation == destination) IsFinishedAnimating = true;
            if (updateAction != null) updateAction();
            if (IsFinishedAnimating && endAction != null) endAction();
        }
    }
}