﻿using System;
using UnityEngine;

namespace utils
{
    /// <summary>
    /// Encapsulates the animation of the column trigger
    /// </summary>
    public class MoveAnimation
    {
        private float speed;
        private float direction;
        private Predicate<float> endCondition;
        private Action endAction;
        private Action updateAction;
        private Transform parentTransform;
        private Vector3 destination;
        public bool IsFinishedAnimating { get; set; }

        public MoveAnimation(Transform parentTransform, float speed, Vector3 destination, Action endAction,
            Action updateAction = null)
        {
            this.parentTransform = parentTransform;
            this.speed = speed;
            this.destination = destination;
            this.endAction = endAction;
            this.updateAction = updateAction;
        }


        /// <summary>
        /// Update must be called every frame in order to animate
        /// </summary>
        public void Update()
        {
            if (IsFinishedAnimating) return;
            var step = speed * Time.deltaTime;
            parentTransform.position = Vector3.MoveTowards(parentTransform.position, destination, step);
            if (Vector3.Distance(parentTransform.position, destination) < 0.001) IsFinishedAnimating = true;
            if (updateAction != null) updateAction();
            if (IsFinishedAnimating && endAction != null) endAction();
        }
    }
}