﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Serialization;
using stats;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Utils.Mechanics.Triggers.Controlled;

public class TurnManager : MonoBehaviour
{
//    public static TurnManager INSTANCE;

    /// <summary>
    /// All the objects in the environment may take an action when turn ends
    /// Order of added action takers will dictate their execution
    /// </summary>
    private List<ITurnEndActionTaker> actionTakersTurnEnd = new List<ITurnEndActionTaker>();

    /// <summary>
    /// All the objects in the environment may take an action when turn starts
    /// Order of added action takers will dictate their execution
    /// </summary>
    private List<ITurnStartActionTaker> actionTakersTurnStart = new List<ITurnStartActionTaker>();


    /// <summary>
    /// All the objects that can undo their action that was taken on previous turn
    /// </summary>
    private List<UndoComponent> undoableObjects = new List<UndoComponent>();

    private int currentTurn;

    public static bool isUndoWasPressed;


    public interface ITurnStartActionTaker
    {
        void TakeTurnStartAction();
    }

    public interface ITurnEndActionTaker
    {
        IObservable<GameObject> TakeTurnEndsAction();

        /// <summary>
        /// Defines who takes the end turn action first
        /// </summary>
        /// <returns></returns>
        int GetTurnEndPriority();

        /// <summary>
        /// Called after player started his last move but no other turn end action takers
        /// yet have made theirs
        /// </summary>
        void TurnWillEnd();
    }

//    public TurnManager()
//    {
//        INSTANCE = this;
//    }

    // Use this for initialization
    void Awake()
    {
        actionTakersTurnEnd.Clear();
        actionTakersTurnStart.Clear();
        undoableObjects.Clear();
        undoableObjects.AddRange(FindObjectsOfType<UndoComponent>());
    }

//    private void OnDisable()
//    {
//        actionTakersTurnEnd.Clear();
//        actionTakersTurnStart.Clear();
//        undoableObjects.Clear();
//    }


    void Start()
    {
        //FIXME : find a better solution and place to start a turn
        Invoke("NextTurn", 0.3f);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    public void OnPlayerFinishedTurn(GameObject gameObject)
    {
        Invoke("ProcessTurnEnd", 0.2f);
    }

    private void ProcessTurnEnd()
    {
        var enumr = actionTakersTurnEnd.ToList().GetEnumerator();

        if (enumr.MoveNext()) ProcessNextEndTaker(enumr);
        else NextTurn();
    }

    private void ProcessNextEndTaker(List<ITurnEndActionTaker>.Enumerator enumr)
    {
        //Debug.Log("Waiting for + " + enumr.Current + "to Complete his turn");
        enumr.Current.TakeTurnEndsAction()
            .Subscribe(
                x => Debug.Log("Next"),
                e => Debug.Log("Error"),
                () =>
                {
                    //Debug.Log(enumr.Current + " Completed his turn");
                    if (enumr.MoveNext()) ProcessNextEndTaker(enumr);
                    else NextTurn();
                }
            );
    }


    public void OnPlayerWillMakeMove()
    {
        isUndoWasPressed = false;
        //save state for undo purposes
        foreach (var undoComponent in undoableObjects)
            undoComponent.pushUndoState();

        //noify all that turn will end
        foreach (var actionTaker in actionTakersTurnEnd)
            actionTaker.TurnWillEnd();
    }

    public void UndoLastTurn()
    {
        isUndoWasPressed = true;
        foreach (UndoComponent undoComponent in undoableObjects)
            undoComponent.popUndoState();
    }

    private void NextTurn()
    {
        currentTurn++;

        foreach (var actionTaker in actionTakersTurnStart)
            actionTaker.TakeTurnStartAction();

        print("staring turn " + currentTurn);
        actionTakersTurnEnd = SortActionEndTakers(actionTakersTurnEnd);
    }

    private List<ITurnEndActionTaker> SortActionEndTakers(IEnumerable<ITurnEndActionTaker> turnEndActionTakers)
    {
        return turnEndActionTakers
            .OrderByDescending(taker => taker.GetTurnEndPriority())
            .ToList();
    }

    public void AddTurnEndActionTaker(ITurnEndActionTaker actionTaker)
    {
        actionTakersTurnEnd.Add(actionTaker);
    }

    public void RemoveTurnEndActionTaker(ITurnEndActionTaker actionTaker)
    {
        actionTakersTurnEnd.Remove(actionTaker);
    }


    public void RemoveTurnStartActionTaker(ITurnStartActionTaker actionTaker)
    {
        actionTakersTurnStart.Remove(actionTaker);
    }

    public void AddTurnStartActionTaker(ITurnStartActionTaker actionTaker)
    {
        actionTakersTurnStart.Add(actionTaker);
    }

//    public IObservable<GameObject> createTrunEndSynchingObservable()
//    {
////        var observer = Observer.Create<GameObject>(
////            go =>
////            {
////                //onNext
////            }, exception =>
////            {
////                //onError
////            }, () =>
////            {
////                //onComplete
////            });
//
////        return Observable.Create<GameObject>(observer1 => null);
//        return Observable.Empty<GameObject>();
//    }
}