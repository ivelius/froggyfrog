﻿using System.Collections;
using System.Collections.Generic;
using stats;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class GameRulesComponent : MonoBehaviour, TurnManager.ITurnEndActionTaker, UndoComponent.IUndoStateCreator
{
    public Text timerText;
    public int totalTurnsAvailible;

    internal class UndoState : UndoComponent.IUndoState
    {
        internal readonly int totalTurnsAvailible;

        public UndoState(int totalTurnsAvailible)
        {
            this.totalTurnsAvailible = totalTurnsAvailible;
        }
    }

    private void Awake()
    {
//        totalTurnsAvailible = 50;
        UpdateTimer();

        //TODO : If there is no energy , level should not start !
        var energy = StatsManager.GetIntStatFromPreferences(StatsManager.Type.ENERGY);
        StatsManager.StoreIntStatInPreferences(StatsManager.Type.ENERGY, energy - 1);
            
    }

    private void UpdateTimer()
    {
        timerText.text = totalTurnsAvailible.ToString();
    }

    // Use this for initialization
    void Start()
    {
        FindObjectOfType<TurnManager>().AddTurnEndActionTaker(this);

        if (StatsManager.GetIntStatFromPreferences(StatsManager.Type.ENERGY) <= 0)
            FindObjectOfType<HudEventsDispather>().showNoEnergyPopup();
        
        if (StatsManager.GetIntStatFromPreferences(StatsManager.Type.LIVES) <= 0)
            FindObjectOfType<HudEventsDispather>().showNoLivesPopup();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public IObservable<GameObject> TakeTurnEndsAction()
    {
        totalTurnsAvailible--;
        if (totalTurnsAvailible <= 0)
        {
            NotificationCenter.DefaultCenter.PostNotification(this,
                PlayerComponent.PLAYER_DEAD_EVENT);
            return Observable.Empty<GameObject>();
        }

        UpdateTimer();
        return Observable.Empty<GameObject>();
    }

    public int GetTurnEndPriority()
    {
        return 0;
    }

    public UndoComponent.IUndoState createUndoState()
    {
        return new UndoState(totalTurnsAvailible);
    }

    public void OnUndoButtonClicked()
    {
        FindObjectOfType<TurnManager>().UndoLastTurn();
    }

    public void undoState(UndoComponent.IUndoState state)
    {
        var undoState = state as UndoState;
        totalTurnsAvailible = undoState.totalTurnsAvailible;
        UpdateTimer();
    }

    public void TurnWillEnd()
    {
    }
}