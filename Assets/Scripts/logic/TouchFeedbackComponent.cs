﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class TouchFeedbackComponent : MonoBehaviour
{
    public Material feedbackMaterial;
    private Material defaultMaterial;
    public float afterTouchSeconds;
    private bool isTouching;

    private void Awake()
    {
        defaultMaterial = GetComponent<Renderer>().material;
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {

            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit[] hits;
            hits = Physics.RaycastAll(ray);

            for (int i = 0; i < hits.Length; i++)
            {
                RaycastHit hit = hits[i];
                if (hit.collider.gameObject.Equals(gameObject)) OnTouchStarted();
            }
        }

        if (Input.GetMouseButtonUp(0) && isTouching) OnTouchEnded();
    }

    private void OnTouchEnded()
    {
        isTouching = false;
        Invoke("SetDefaultMaterial", afterTouchSeconds);
    }

    private void OnTouchStarted()
    {
        CancelInvoke();
        isTouching = true;
        SetMaterial(feedbackMaterial);
    }

    private void SetMaterial(Material material)
    {
        GetComponent<Renderer>().material = material;
    }

    private void SetDefaultMaterial()
    {
        SetMaterial(defaultMaterial);
    }
}