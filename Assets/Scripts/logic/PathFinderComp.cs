﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utils;

public class PathFinderComp : MonoBehaviour
{
    public const float MIN_JUMP_RADIUS = 1f;
    public const float MAX_JUMP_RADIUS = 5f;

    [Range(MIN_JUMP_RADIUS, MAX_JUMP_RADIUS)] public float jumpRadius = 1f;
    public Color reachableHighlightColor = Color.blue;

    private Dictionary<Renderer, Color> renToColorMap = new Dictionary<Renderer, Color>();


    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    /// <summary>
    /// Returns all potential reachable jump points excluding the one
    /// we are standing on. That also includes points occupied by other charachters
    /// </summary>
    /// <returns></returns>
    public JumpPointComponent[] FindReachableJumpPoints()
    {
        //jump point that we are standing on
        var currentJumpPointComp = FrogUtils.FindGameObjectsAtPosition(transform.position, 1f)
            .Where(o => o.GetComponent<JumpPointComponent>() != null )
            .Select(o => o.GetComponent<JumpPointComponent>())
            .FirstOrDefault();

        if (currentJumpPointComp == null) return new JumpPointComponent[] { };

        return FrogUtils.FindGameObjectsAtPosition(transform.position,
                jumpRadius)
            //Filter only jumpable surfaces
            .Where(o => o.GetComponent<JumpPointComponent>() != null)
            .Where(o => o.GetComponent<JumpPointComponent>().enabled)
            //Transform to component
            .Select(o => o.GetComponent<JumpPointComponent>())
            //Filter current surface
            .Except(new[] {currentJumpPointComp})
            .ToArray();
    }

    private void OnDrawGizmosSelected()
    {
    }

    public void HighlightReachablePlatforms()
    {
        //hightlight reachable surfaces
        var points = GetComponent<PathFinderComp>().FindReachableJumpPoints();
        changeEmmisionLight(points, reachableHighlightColor);
    }

    private void changeEmmisionLight(JumpPointComponent[] points, Color color)
    {
        foreach (var point in points)
        {
            Renderer renderer = point.GetComponentInChildren<MeshRenderer>();
            if (renderer == null) renderer = point.GetComponent<Renderer>();
            Material mat = renderer.material;
            renToColorMap[renderer] = mat.GetColor("_EmissionColor");
            mat.SetColor("_EmissionColor", color);
        }
    }

    public void UnHighlightReachablePlatforms()
    {
        //unhightlight reachable surfaces
        var points = GetComponent<PathFinderComp>().FindReachableJumpPoints();
        foreach (var point in points)
        {
            Renderer renderer = point.GetComponentInChildren<MeshRenderer>();
            if (renderer == null) renderer = point.GetComponent<Renderer>();
            if (!renToColorMap.ContainsKey(renderer)) continue;
            Material mat = renderer.material;
            mat.SetColor("_EmissionColor", renToColorMap[renderer]);
        }
    }
}