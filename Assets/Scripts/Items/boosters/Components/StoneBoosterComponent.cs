﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils.Items;

[RequireComponent(typeof(PlayerComponent))]
public class StoneBoosterComponent : MonoBehaviour
{

    private GameObject stonePrefab;
    public StoneBooster stoneBoosterItem;

    private void Awake()
    {
        stonePrefab = Resources.Load("Prefabs/ThrowStonePrefab") as GameObject;
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update(){

        if (Input.GetMouseButtonUp(0))
        {
            var ray2 = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit2;
            if (Physics.Raycast(ray2, out hit2))
            {
                var touchPoint = hit2.collider.GetComponent<JumpPointComponent>();
                if (touchPoint == null || !touchPoint.enabled) return;

                //throw item :
                var throwStone = Instantiate(stonePrefab);
                throwStone.transform.position = new Vector3(transform.position.x,
                    transform.position.y + 1, transform.position.z);

                var stoneJumperComponent = throwStone.GetComponent<JumperComponent>();
                stoneJumperComponent.StartJump(touchPoint.getJumpTouchPoint(), null);

                GetComponent<PlayerComponent>().OnItemUsed(stoneBoosterItem);

            }
        }
    }

    public void ActivateItemUsage(string itemToUse)
    {
        //TODO : maybe notyfying hud here is better ?
    }

    public void DeactivateItemUsage()
    {
        //TODO
    }
}