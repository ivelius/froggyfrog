﻿using UnityEngine;

namespace Utils.Items
{
    public interface IBooster
    {
        void AddBoosterComponent(GameObject go);
        void RemoveBoosterComponent(GameObject go);
    }
}