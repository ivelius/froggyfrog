﻿using UnityEngine;

namespace Utils.Items
{
    public class StoneBooster : IBooster, ICollectableItem
    {
        public void AddBoosterComponent(GameObject go)
        {
            go.AddComponent<StoneBoosterComponent>();
            go.GetComponent<StoneBoosterComponent>().stoneBoosterItem = this;
        }

        public void RemoveBoosterComponent(GameObject go)
        {
            Object.Destroy(go.GetComponent<StoneBoosterComponent>());
        }
    }
}