﻿using UnityEngine;

namespace Utils.Items
{
    public interface IItemsCollector
    {
        void onItemCollected(ICollectableItem item);
    }
}