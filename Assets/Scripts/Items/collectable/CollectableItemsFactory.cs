﻿using Utils.Items.Collectables;

namespace Utils.Items
{
    public class CollectableItemsFactory
    {
        public static ICollectableItem createFromEnum(CollectibleItems item)
        {
            switch (item)
            {
                case CollectibleItems.THROW_STONE:
                    return new StoneBooster();
                case CollectibleItems.SPECIAL_LILLY:
                    return new Screw();
                case CollectibleItems.BUTTERFLY:
                    return new Butterfly();

                default:
                    throw new System.Exception("Unknow item " + item);
            }
        }
    }
}