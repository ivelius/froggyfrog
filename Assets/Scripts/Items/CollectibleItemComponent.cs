﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils.Items;
using Utils.Items.Collectables;

public class CollectibleItemComponent : MonoBehaviour
{
    public float rotationSpeed = 160;
    public CollectibleItems item;


    // Use this for initialization
    void Start()
    {
        if (item == CollectibleItems.BUTTERFLY)
        {
            var script = FindObjectOfType<ButterfliesUiScript>();
            if (script != null) script.addPlaceHolder();
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(transform.position, transform.up, Time.deltaTime * rotationSpeed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<IItemsCollector>() != null)
        {
            var collector = other.gameObject.GetComponent<IItemsCollector>();
            collector.onItemCollected(CollectableItemsFactory.createFromEnum(item));
            Destroy(gameObject);
        }
    }
}