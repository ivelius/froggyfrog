﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class ButterfliesUiScript : MonoBehaviour
{
    private int collectedButterflies;
    public Text winPopupButterflyCountText;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void addButterFlyAsCollected()
    {
        if (collectedButterflies >= gameObject.transform.childCount) return;

        var butterflyImage = gameObject.transform.GetChild(collectedButterflies).transform.GetChild(0);
        butterflyImage.gameObject.SetActive(true);
        collectedButterflies++;
        winPopupButterflyCountText.text = "" + collectedButterflies;
    }

    public void addPlaceHolder()
    {
        var placeholder = FrogUtils.InstantiatePrefab("ui/ButterflyUiIcon");
        placeholder.transform.SetParent(gameObject.transform);
    }
}