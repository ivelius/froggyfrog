﻿using UnityEngine;

public class JumperComponent : MonoBehaviour
{
    public float jumpSpeed = 5.0f;
    private bool isInAir;
    private float jumpStartTime;
    public float jumpHeight = 1f;
    private Vector3 jumpStartPosition;
    private Vector3 jumpEndPosition;
    private Vector3 jumpHighestPointPosition;

    public AudioClip jumpStartSound;
    public AudioClip jumpEndSound;
    private IJumpListener jumpListener;


    public interface IJumpListener
    {
        void OnJumpStart();
        void OnJumpEnd();
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (isInAir)
        {
            ProcessJump();
        }
    }


    private void ProcessJump()
    {
        var percentage = (Time.time - jumpStartTime) / jumpSpeed;
        var newPos = GetBezierPoint(jumpStartPosition, jumpHighestPointPosition, jumpEndPosition, percentage);
        gameObject.transform.position = newPos;
        if (Time.time - jumpStartTime > jumpSpeed) EndJump();
    }

    private void EndJump()
    {
        if (jumpEndSound != null) GetComponent<AudioSource>().PlayOneShot(jumpEndSound);

        isInAir = false;

        if (jumpListener != null) jumpListener.OnJumpEnd();
        jumpListener = null;
    }

    /// <summary>
    /// Start the jump from current position.
    /// </summary>
    /// <param name="destination"></param>
    public void StartJump(Vector3 destination, IJumpListener listener)
    {
        if (jumpStartSound != null) GetComponent<AudioSource>().PlayOneShot(jumpStartSound);
        jumpListener = listener;

        isInAir = true;
        jumpStartTime = Time.time;
        jumpStartPosition = gameObject.transform.position;
        jumpEndPosition = destination;
        var x = jumpStartPosition.x + (jumpEndPosition.x - jumpStartPosition.x) / 2;
        var z = jumpStartPosition.z + (jumpEndPosition.z - jumpStartPosition.z) / 2;
        jumpHighestPointPosition = new Vector3(x, jumpHeight, z);
        if (jumpListener != null) jumpListener.OnJumpStart();
    }


    public void RotateTowardsDestination(Vector3 destination)
    {
        var transformForward = Vector3.Normalize(destination - transform.position);
        transformForward.y = 0;
        transform.forward = transformForward;
    }

    private Vector3 GetBezierPoint(Vector3 p0, Vector3 p1, Vector3 p2, float percentage)
    {
        return Vector3.Lerp(Vector3.Lerp(p0, p1, percentage), Vector3.Lerp(p1, p2, percentage), percentage);
    }
}