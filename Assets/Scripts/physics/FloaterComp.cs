﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloaterComp : MonoBehaviour
{
    public float minFloatingSpeed = 0.001f;
    public float maxFloatingSpeed = 0.01f;
    public float minRotationSpeedX = 0.001f;
    public float maxRotationSpeedX = 0.01f;
    public float minRotationSpeedY = 0.001f;
    public float maxRotationSpeedY = 0.01f;
    public float minYPos = -0.03f;
    public float maxYPos = 0.02f;
    public float minZRotation = -2f;
    public float maxZRoation = 2f;
    public float minYRotation = -2f;
    public float maxYRoation = 2f;
    public float floatSpeed;
    public float rotationSpeedX;
    public float rotationSpeedY;
    private int posDirection = -1;
    private int rotDirectionX = -1;
    private int rotDirectionY = -1;

    void Start()
    {
        Randomie();
    }

    void Update()
    {
        ChangePosition(posDirection);
        ChangeRotation(rotDirectionX);

        //flip posDirection on borders
        if (transform.position.y <= minYPos) posDirection *= -1;
        if (transform.position.y >= maxYPos) posDirection *= -1;

        //flip posDirection on borders
        if (transform.rotation.eulerAngles.y <= minZRotation) rotDirectionX *= -1;
        if (transform.rotation.eulerAngles.y >= maxZRoation) rotDirectionX *= -1;

        if (transform.rotation.eulerAngles.z <= minZRotation) rotDirectionY *= -1;
        if (transform.rotation.eulerAngles.z >= maxZRoation) rotDirectionY *= -1;
    }

    private void Randomie()
    {
        floatSpeed = Random.Range(minFloatingSpeed, maxFloatingSpeed);
        rotationSpeedX = Random.Range(minRotationSpeedX, maxRotationSpeedX);
        rotationSpeedY = Random.Range(minRotationSpeedY, maxRotationSpeedY);
        rotDirectionX = Random.Range(0, 11) < 5 ? 1 : -1;
    }

    private void ChangePosition(int direction)
    {
        transform.position = new Vector3(transform.position.x,
            transform.position.y + (direction * floatSpeed * Time.deltaTime),
            transform.position.z);
    }

    private void ChangeRotation(int direction)
    {
        var rotationAngles = new Vector3(0,
            rotDirectionY * rotationSpeedY * Time.deltaTime, 0 + (direction * rotationSpeedX * Time.deltaTime));
        transform.Rotate(rotationAngles);
    }
}