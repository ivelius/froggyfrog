﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPointComponent : MonoBehaviour
{
    public Color color = Color.red;
    public float touchPointOffset = 0;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = color;
        Gizmos.DrawSphere(getJumpTouchPoint(), 0.1f);
    }

    public Vector3 getJumpTouchPoint()
    {
        return new Vector3(gameObject.transform.position.x,
            gameObject.transform.position.y + touchPointOffset, gameObject.transform.position.z);
    }
}