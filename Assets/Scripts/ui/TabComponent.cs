﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class TabComponent : MonoBehaviour
{
    public Color enabledColor;
    public Color disabledColor;
    public Color enabledIconColor;
    public Color disabledIconColor;
    public Image iconImage;

    // Use this for initialization
    void Start()
    {
        var tabGroup = GetComponentInParent<TabGroup>();
        if (tabGroup == null) return;
        GetComponent<Button>().onClick.AddListener(() => tabGroup.onTabSelected(this));
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void SetTabActive(bool active)
    {
        if (active) SetActive();
        else SetInactive();
    }

    public void SetActive()
    {
        gameObject.GetComponent<Image>().color = enabledColor;
        iconImage.color = enabledIconColor;
    }

    public void SetInactive()
    {
        gameObject.GetComponent<Image>().color = disabledColor;
        iconImage.color = disabledIconColor;
    }
}