﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
[RequireComponent(typeof(Image))]
public class MyUIToggle : MonoBehaviour
{
    public Color tintColorOn;
    [Range(0.1f, 1f)] public float disabledAlpha;
    public bool isInteractive;
    public bool isOn;
    private Button btnComp;
    private Image img;
    public Image[] controlledGraphics;
    public Toggle.ToggleEvent onValueChanged = new Toggle.ToggleEvent();

    private void Awake()
    {
        btnComp = GetComponent<Button>();
        img = GetComponent<Image>();

        //this toggle will respond to button click events
        btnComp.onClick.AddListener(Toggle);
    }

    public bool IsInteractive
    {
        get { return isInteractive; }
        set
        {
            if (!value) IsOn = false;
            isInteractive = value;

            if(btnComp == null) return;
            btnComp.interactable = isInteractive;

            //we will asssue that default tint on every graphic is always white
            //TODO : If not , then the better way would be to attach this toggle component to
            //TODO : every child ui component that we want to toggle
            //TODO : Or create a toggle slave component
            var color = isInteractive
                ? Color.white
                : new Color(Color.white.r, Color.white.g, Color.white.b, disabledAlpha);

            ChangeColor(color);

            //TODO : Notify that interaction is changed ???
        }
    }

    public bool IsOn
    {
        get { return isOn; }
        set
        {
            isOn = value;

            var tint = isOn ? tintColorOn : Color.white;
            ChangeColor(tint);

            //Notify that interaction is changed ???
            onValueChanged.Invoke(isOn);
        }
    }

    private void ChangeColor(Color tint)
    {
        if(img == null) return;
        img.color = tint;
        foreach (var g in controlledGraphics) g.color = tint;
    }

    public void Toggle()
    {
        if (!isInteractive) return;
        IsOn = !isOn;
    }


    // Use this for initialization
    void Start()
    {
        IsInteractive = isInteractive;
        if (!isInteractive) return;
        IsOn = isOn;
    }

    // Update is called once per frame
    void Update()
    {

    }
}