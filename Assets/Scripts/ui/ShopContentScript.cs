﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class ShopContentScript : MonoBehaviour
{
    private static string PREFABS_PATH = "shop/";

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void PopulateWithPurchasables(string purchasableName)
    {
        foreach (Transform child in gameObject.transform)
            Destroy(child.gameObject);

        for (var i = 0; i < 10; i++)
        {
            var child = FrogUtils.InstantiatePrefab(PREFABS_PATH + purchasableName);
            child.transform.SetParent(gameObject.transform);
        }
    }
}