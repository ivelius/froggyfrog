﻿using stats;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelMapButtonScript : MonoBehaviour
{
    public ProgressionManager.Level level;
    public GameObject[] stars;
    public Text levelText;

    private void Awake()
    {
        levelText.text = level.ToString().Remove(0, 1);
        GetComponent<Button>().onClick.AddListener(() => LoadLevel("level" + level));
    }


    // Use this for initialization
    void Start()
    {
        InitStars();
    }

    public void InitStars()
    {
        foreach (var star in stars) star.SetActive(false);

        var range = ProgressionManager.GetStarsForLevel(level);
        for (var i = 0; i < range; i++)
            stars[i].SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnDrawGizmos()
    {
        levelText.text = level.ToString().Remove(0, 1);
    }

    public void LoadLevel(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }
}