﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using stats;
using UnityEngine;
using Utils.Items;

public class HudEventsDispather : MonoBehaviour
{
    public ProgressionManager.Level level;
    public GameObject gameOverPopup;
    public GameObject youWinPopup;

    public GameObject noLivesPopup;
    public GameObject noEnergyPopup;

    private void Awake()
    {
        Time.timeScale = 1f;
    }

    // Use this for initialization
    void Start()
    {
        NotificationCenter.DefaultCenter.AddObserver(this,
            PlayerComponent.PLAYER_DEAD_EVENT);

        NotificationCenter.DefaultCenter.AddObserver(this,
            PlayerComponent.PLAYER_WIN_EVENT);
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void showNoEnergyPopup()
    {
        noEnergyPopup.SetActive(true);
        Time.timeScale = 0;
    }

    public void showNoLivesPopup()
    {
        noLivesPopup.SetActive(true);
        Time.timeScale = 0;
    }

    public void ToggleStoneSelected(bool isSelected)
    {
        var playerComponent = FindObjectOfType<PlayerComponent>()
            .GetComponent<PlayerComponent>();

        var backpack = FindObjectOfType<BackPackComponent>()
            .GetComponent<BackPackComponent>();

        var collectableItem = backpack.GetItemsOfType<StoneBooster>().FirstOrDefault();
        if (collectableItem == null) return;

        var stoneBooster = collectableItem as StoneBooster;
        if (isSelected) playerComponent.UseBooster(stoneBooster);
        else playerComponent.DefaultState();
    }


    public void OnPlayerDied(NotificationCenter.Notification aNotification)
    {
        gameOverPopup.SetActive(true);
        Time.timeScale = 0;
    }

    public void OnPlayerWin(NotificationCenter.Notification aNotification)
    {
        youWinPopup.SetActive(true);
        Time.timeScale = 0;

        var turnsLeft = FindObjectOfType<GameRulesComponent>().totalTurnsAvailible;
        var currentStarsForLevel = turnsLeft > 6 ? 3 : (turnsLeft > 4) ? 2 : 1;
        ProgressionManager.SetStarsForLevel(level, Math.Max(currentStarsForLevel,ProgressionManager.GetStarsForLevel(level)));

        youWinPopup.GetComponent<StarAchievmentScript>().ShowAchievedStars(currentStarsForLevel);
    }
}