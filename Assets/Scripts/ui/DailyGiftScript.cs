﻿using System.Collections;
using System.Collections.Generic;
using utils;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class DailyGiftScript : MonoBehaviour
{
    public RectTransform top;
    public GameObject prestent;


    private void Awake()
    {
        GetComponent<Button>().onClick.AddListener(OpenGift);
    }

    private void OpenGift()
    {
        //TODO : 
//        top.position = new Vector2(top.position.x, 0);
        top.localPosition = new Vector2(top.localPosition.x, 100);
        prestent.SetActive(true);
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }
}