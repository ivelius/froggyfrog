﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TabGroup : MonoBehaviour
{
    private List<TabComponent> tabs;

    // Use this for initialization
    void Start()
    {
        tabs = GetComponentsInChildren<TabComponent>().ToList();
        tabs.ForEach(component => component.SetInactive());
        tabs[0].SetActive();
        tabs[0].gameObject.GetComponent<Button>().onClick.Invoke();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void onTabSelected(TabComponent tab)
    {
        tabs.ForEach(tabComp => tabComp.SetTabActive(tabComp == tab));
    }
}