﻿using System.Linq;
using UnityEngine;
using Utils;

/// <summary>
/// This component is responsible for placing surface ojects
/// on the grid
/// </summary>
[RequireComponent(typeof(MapEditorGridComp))]
public class MapEditorSurfacePlacerComp : MonoBehaviour
{
    public Mesh SelectedMesh { get; set; }
    public GameObject container;


    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnDrawGizmosSelected()
    {
        var mapEditorGridComponent = GetComponent<MapEditorGridComp>();
        if (SelectedMesh == null
            || !mapEditorGridComponent.isMouseHoveringGrid) return;
        Gizmos.DrawMesh(SelectedMesh, mapEditorGridComponent.mouseHoverPosition);
    }

    public void PlaceObjectOnGrid(GameObject loadPrefab)
    {
        var mapEditorGridComp = GetComponent<MapEditorGridComp>();
        if (!mapEditorGridComp.isMouseHoveringGrid) return;

        //we will destroy whatever object was at current position before we add a new one
        DeleteObjectOnCurrentPosition();
        var instantiatedObject = Instantiate(loadPrefab);
        instantiatedObject.transform.position = mapEditorGridComp.mouseHoverPosition;
#if UNITY_EDITOR
        UnityEditor.Undo.RegisterCreatedObjectUndo(instantiatedObject, "Create " + loadPrefab.name);
#endif

        //parent this object somewhere to easy access later
        instantiatedObject.transform.SetParent(container.transform);
    }

    public void DeleteObjectOnCurrentPosition()
    {
#if UNITY_EDITOR
        var pos = GetComponent<MapEditorGridComp>().mouseHoverPosition;
        var collidedGameObjects = FrogUtils.FindGameObjectsAtPosition(pos);

        foreach (var collidedGameObject in collidedGameObjects)
            UnityEditor.Undo.DestroyObjectImmediate(collidedGameObject.gameObject);
#endif
    }
}