﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class MapEditorGridComp : MonoBehaviour
{
    public const float OBJECT_PLACEMENT_OFFSET = 0.5f;

    public int gridWidth;
    public int gridLength;
    public bool isMouseHoveringGrid;

    private Color gridColorDisabled = Color.gray;
    private Color gridColorSelected = Color.white;
    internal Vector3 mouseHoverPosition;


    // Use this for initialization
    void Start()
    {
        //we don't need this object at gam time , so
        //we just disable it
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnDrawGizmosSelected()
    {
        DrawGrid(gridColorSelected);

        //when mouse is hovering over grid we will draw its position
        if (isMouseHoveringGrid) DrawMousePos();
    }

    private void OnDrawGizmos()
    {
        DrawGrid(gridColorDisabled);
    }


    private void DrawMousePos()
    {
        Gizmos.color = Color.white;
        //In order to draw a cube at the correct spot , we need to adust it to the mouse position
        Gizmos.DrawCube(mouseHoverPosition, new Vector3(1, 0.01f, 1));
    }

    /// <summary>
    /// Simply draw a grid on the scene according to selected dimentions.
    /// </summary>
    /// <param name="gridColor">Color of the grid mesh</param>
    private void DrawGrid(Color gridColor)
    {
        Gizmos.color = gridColor;
        var xBorder = (gridWidth + transform.position.x);
        var zBorder = (gridLength + transform.position.z);
        for (var x = transform.position.x; x <= xBorder; x++)
        {
            for (var z = transform.position.z; z <= zBorder; z++)
            {
                Gizmos.DrawLine(new Vector3(x, 0, z), new Vector3(x, 0, zBorder));
                Gizmos.DrawLine(new Vector3(x, 0, z), new Vector3(xBorder, 0, z));
            }
        }
    }

    public void SetMouseHoverPosition(Vector3 pos)
    {
        isMouseHoveringGrid = true;
        //correct hovering position to objects offset
        mouseHoverPosition = new Vector3(pos.x + OBJECT_PLACEMENT_OFFSET,
            pos.y,
            pos.z + OBJECT_PLACEMENT_OFFSET);
    }

    /// <summary>
    /// This method synchronizes the position of the collider with the grid dimentions
    /// </summary>
    public void UpdateColliderBoundries()
    {
        var boxCollider = GetComponent<BoxCollider>();
        boxCollider.center = new Vector3(
            gridWidth * OBJECT_PLACEMENT_OFFSET,
            boxCollider.center.y,
            gridLength * OBJECT_PLACEMENT_OFFSET);
        boxCollider.size = new Vector3(gridWidth, boxCollider.size.y, gridLength);
    }

    public void SetMouseHover(bool isMouseHoveringGrid)
    {
        this.isMouseHoveringGrid = isMouseHoveringGrid;
    }
}