﻿using System.Linq;
using UnityEngine;
using Utils;

[RequireComponent(typeof(MapEditorGridComp))]
[RequireComponent(typeof(MapEditorSurfacePlacerComp))]
public class MapEditorItemsPlacerComp : MonoBehaviour
{
    public Mesh SelectedMesh { get; set; }
    public GameObject container;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }


    private void OnDrawGizmosSelected()
    {
        var mapEditorGridComponent = GetComponent<MapEditorGridComp>();
        if (SelectedMesh == null
            || !mapEditorGridComponent.isMouseHoveringGrid) return;

        //find all objects at hover position
        var collidedGameObjects = FrogUtils.FindGameObjectsAtPosition(mapEditorGridComponent.mouseHoverPosition, 0.3f);
        if (collidedGameObjects == null) return;

        var go = collidedGameObjects
            .FirstOrDefault(o => o.GetComponent<JumpPointComponent>());

        if (go == null) return;
        var jumpPointComp = go.GetComponent<JumpPointComponent>();

        var pos = new Vector3(mapEditorGridComponent.mouseHoverPosition.x,
            mapEditorGridComponent.mouseHoverPosition.y + jumpPointComp.touchPointOffset,
            mapEditorGridComponent.mouseHoverPosition.z);

        Gizmos.DrawMesh(SelectedMesh, pos);
    }

    public void PlaceObjectOnGrid(GameObject loadPrefab)
    {
        var mapEditorGridComp = GetComponent<MapEditorGridComp>();
        if (!mapEditorGridComp.isMouseHoveringGrid) return;

        //we will destroy whatever object was at current position before we add a new one
        DeleteObjectOnCurrentPosition();

        var instantiatedObject = Instantiate(loadPrefab);
        var placePosition = FindPlacemntPosition();
        instantiatedObject.transform.position = placePosition;
#if UNITY_EDITOR
        UnityEditor.Undo.RegisterCreatedObjectUndo(instantiatedObject, "Create " + loadPrefab.name);
#endif


        //parent this object somewhere to easy access later
        instantiatedObject.transform.SetParent(container.transform);
    }

    private Vector3 FindPlacemntPosition()
    {
        var mousePos = GetComponent<MapEditorGridComp>().mouseHoverPosition;
        var jumpPointComp = FrogUtils.FindGameObjectsAtPosition(mousePos)
            .Where(go => go.GetComponent<JumpPointComponent>() != null)
            .Select(o => o.GetComponent<JumpPointComponent>())
            .FirstOrDefault();
        return new Vector3(mousePos.x, mousePos.y + jumpPointComp.touchPointOffset, mousePos.z);
    }

    public void DeleteObjectOnCurrentPosition()
    {
        var pos = GetComponent<MapEditorGridComp>().mouseHoverPosition;
        var collidedGameObjects = FrogUtils.FindGameObjectsAtPosition(pos, 1.5f);

        foreach (var collidedGameObject in collidedGameObjects)
        {
#if UNITY_EDITOR
            //TODO: DO NOT RELY ON THIS COMPONENT !!!!!
            if (collidedGameObject.GetComponent<JumperComponent>() != null)
            {
                UnityEditor.Undo.DestroyObjectImmediate(collidedGameObject.gameObject);
            }
#endif
        }
    }
}