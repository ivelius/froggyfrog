﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(IUndoStateCreator))]
public class UndoComponent : MonoBehaviour
{
    private Stack<IUndoState> undoStatesForTurn = new Stack<IUndoState>();

    public interface IUndoStateCreator
    {
        IUndoState createUndoState();
        void undoState(IUndoState state);
    }

    public interface IUndoState
    {
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void pushUndoState()
    {
        //Debug.Log("Pushing Undo State");
        var undoState = GetComponent<IUndoStateCreator>().createUndoState();
        if (undoState == null) return;
        undoStatesForTurn.Push(undoState);
    }

    public void popUndoState()
    {
        //Debug.Log("Popping Undo State");
        if (undoStatesForTurn.Count == 0) return;
        var undoState = undoStatesForTurn.Pop();
        GetComponent<IUndoStateCreator>().undoState(undoState);
    }
    
//    public void pushUndoState()
//    {
//        //Debug.Log("Pushing Undo State");
//        foreach (var component in GetComponents<IUndoStateCreator>())
//        {
//            var undoState = component.createUndoState();
//            if (undoState == null) return;
//            undoStatesForTurn.Push(undoState);
//        }
//    }
//
//    public void popUndoState()
//    {
//        //Debug.Log("Popping Undo State");
//        if (undoStatesForTurn.Count == 0) return;
//        
//        foreach (var component in GetComponents<IUndoStateCreator>())
//        {
//            var undoState = undoStatesForTurn.Pop();
//            if (undoState == null) return;
//            component.undoState(undoState);
//        }
//    }
}