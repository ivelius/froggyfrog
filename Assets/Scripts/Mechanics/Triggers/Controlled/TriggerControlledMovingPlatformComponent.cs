﻿using System;
using System.Linq;
using utils;
using UniRx;
using UnityEngine;
using Utils.Mechanics.Triggers.Platforms.states;

namespace Utils.Mechanics.Triggers.Controlled
{
    public class TriggerControlledMovingPlatformComponent : TriggerControlledObject, TurnManager.ITurnEndActionTaker,
        UndoComponent.IUndoStateCreator
    {
        private const float DRAGGED_OBJECTS_RADIUS = 0.8f;

        private TriggerControlledMovingPlatformState state;
        public Vector3 destination;
        private Vector3 initialPosition;
        public float speed = 5;
        public bool ignoreRotation;

        [Tooltip("Degrees in second")] public float rotationSpeed = 5;
        private Quaternion initialRotation;
        public Vector3[] rotationDestination;


        internal class UndoState : UndoComponent.IUndoState
        {
            internal readonly Vector3 position;
            internal readonly Quaternion rotation;

            public UndoState(Vector3 position, Quaternion rotation)
            {
                this.position = position;
                this.rotation = rotation;
            }
        }

        private void ChangeState(TriggerReactionType type)
        {
            state = TypeToState(type);
        }

        private TriggerControlledMovingPlatformState TypeToState(TriggerReactionType type)
        {
            switch (type)
            {
                case TriggerReactionType.ON_PRESS:
                    return new TriggerControlledMovingPlatformOnPressState(this);

                case TriggerReactionType.ON_UNPRESS:
                    return new TriggerControlledMovingPlatformOnUnPressState(this);

                case TriggerReactionType.ON_ALL:
                    return new TriggerControlledMovingPlatformMixedState(this);

                default:
                    throw new Exception("Unknown trigger type : " + type);
            }
        }

        public MoveAnimation CreateMoveToNextPointAnimation(Action endAction = null)
        {
            
            //Trigger animation if exists
            SetAnimation("move");

            var dest = FrogUtils.IsZroDistance(transform.position, destination) ? initialPosition : destination;
            AddDragComponentsOnDraggedObjects();
            return new MoveAnimation(transform, speed, dest, endAction,
                WrapEndAction(CreateDragObjectsMoveUpdateAction()));
        }

        private void SetAnimation(string trigger)
        {
            var animatior = GetComponent<Animator>();
            if (animatior != null) animatior.SetTrigger(trigger);
        }

        public RotateAnimation CreateNextRotationAnimation(Action endAction = null)
        {
            var dest = FindNextRotation();
            AddDragComponentsOnDraggedObjects();
            return new RotateAnimation(transform, rotationSpeed, dest,
                endAction, WrapEndAction(CreateDragObjectsRotateUpdateAction()));
        }

        private void AddDragComponentsOnDraggedObjects()
        {
            var draggedObjects = FrogUtils.FindGameObjectsAtPosition(transform.position, DRAGGED_OBJECTS_RADIUS);
            foreach (var draggedObject in draggedObjects)
            {
                //skip this platform object
                if (draggedObject.Equals(gameObject)) continue;
                var doc = draggedObject.GetComponent<DraggedObjectComp>();
                if (doc == null) draggedObject.AddComponent<DraggedObjectComp>();
            }
        }

        public Action WrapEndAction(Action endAction)
        {
            return () =>
            {
                //Trigger animation end if exists
                SetAnimation("stop");
                
                endAction();
                //we run the wrapped end action only when both animations are done
                if (state.moveAnimation != null && !state.moveAnimation.IsFinishedAnimating) return;
                if (state.rotateAnimation != null && !state.rotateAnimation.IsFinishedAnimating) return;

                //we destroy all drag components
                DestroyDraggedComponents();
            };
        }

        private void DestroyDraggedComponents()
        {
            var draggedObjects = FrogUtils.FindGameObjectsAtPosition(transform.position, DRAGGED_OBJECTS_RADIUS);
            foreach (var draggedObject in draggedObjects)
            {
                var draggedObjectComp = draggedObject.GetComponent<DraggedObjectComp>();
                if (draggedObjectComp == null) continue;
                draggedObjectComp.enabled = false;
                Destroy(draggedObjectComp);
            }
        }

        /// <summary>
        /// We find the next rotation quaternion that we want to rotate
        /// our platfom towards
        /// </summary>
        /// <returns></returns>
        private Quaternion FindNextRotation()
        {
            var dest = transform.rotation;
            if (rotationDestination == null || rotationDestination.Length == 0) return dest;
            var index = 0;
            foreach (var quaternion in rotationDestination)
            {
                if (dest == Quaternion.Euler(quaternion))
                    return Quaternion.Euler(
                        rotationDestination[(index + 1) % rotationDestination.Length]);
                index++;
            }
            return dest;
        }

        private Action CreateDragObjectsRotateUpdateAction()
        {
            var draggedObjects = FrogUtils.FindGameObjectsAtPosition(transform.position, DRAGGED_OBJECTS_RADIUS);
            var currentPlatformRotation = FrogUtils.CloneQuaternion(transform.rotation).eulerAngles;
            var destPlatformRotation = FindNextRotation().eulerAngles;
            var offsetRotation = destPlatformRotation - currentPlatformRotation;
            return () =>
            {
                foreach (var draggedObject in draggedObjects)
                {
                    //skip this platform object
                    if (draggedObject.Equals(gameObject)) continue;

                    var doc = draggedObject.GetComponent<DraggedObjectComp>();
                    if (doc == null) continue;

                    //we calculate initial rotation of the dragged object
                    //considering the offset of already rotated moving platform
                    var initialRotation = Quaternion.Euler(
                        doc.InitialRotation.eulerAngles - currentPlatformRotation);
                    var transformRotation = initialRotation * transform.rotation;
//                    var transformRotation = doc.InitialRotation * transform.rotation;

                    draggedObject.transform.rotation = transformRotation;
                }
            };
        }

        private Action CreateDragObjectsMoveUpdateAction()
        {
            var draggedObjects = FrogUtils.FindGameObjectsAtPosition(transform.position, DRAGGED_OBJECTS_RADIUS);
            var fromY = transform.position.y;
            return () =>
            {
                foreach (var draggedObject in draggedObjects)
                {
                    //skip this platform object
                    if (draggedObject.Equals(gameObject)) continue;
                    if (draggedObject.GetComponent<SignComponent>() != null) continue;
                    var transformPosition = FrogUtils.CloneVector3(transform.position);
                    //we don't change y value of dragged object if we dragging
                    //only on x and z plane
                    if (Math.Abs(destination.y - initialPosition.y) < 0.1f)
                    {
                        transformPosition.y = draggedObject.transform.position.y;
                    }
                    else
                    {
                        var doc = draggedObject.GetComponent<DraggedObjectComp>();
                        var offset = doc != null ? doc.InitialPosition.y - fromY : 0;
                        transformPosition.y = transform.position.y + offset;
                    }
                    draggedObject.transform.position = transformPosition;
                }
            };
        }

        private void Awake()
        {
            initialPosition = FrogUtils.CloneVector3(transform.position);
            initialRotation = FrogUtils.CloneQuaternion(transform.rotation);
            ChangeState(reactionType);
            state.OnAwake();
        }

        void Start()
        {
//            TurnManager.INSTANCE.AddTurnEndActionTaker(this);
            FindObjectOfType<TurnManager>().AddTurnEndActionTaker(this);
            state.OnStart();
        }

        private void Update()
        {
            state.OnUpdate();
        }


        public override void TriggerPressed()
        {
            state.OnTriggerPressed();
        }

        public override void TriggerUnpressed()
        {
            state.TriggerUnpressed();
        }

        public UniRx.IObservable<GameObject> TakeTurnEndsAction()
        {
            return state.OnTakeTurnEndsAction();
        }

        public int GetTurnEndPriority()
        {
            return 9;
        }

        public UndoComponent.IUndoState createUndoState()
        {
            return new UndoState(FrogUtils.CloneVector3(transform.position),
                FrogUtils.CloneQuaternion(transform.rotation));
        }

        public void undoState(UndoComponent.IUndoState state)
        {
            var undoState = state as UndoState;
            transform.position = undoState.position;
            transform.rotation = undoState.rotation;
        }

        public void TurnWillEnd()
        {
        }
    }
}