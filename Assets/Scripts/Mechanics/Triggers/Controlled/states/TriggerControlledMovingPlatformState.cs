﻿using utils;
using UniRx;
using UnityEngine;
using Utils.Mechanics.Triggers.Controlled;

namespace Utils.Mechanics.Triggers.Platforms.states
{
    public abstract class TriggerControlledMovingPlatformState
    {
        public MoveAnimation moveAnimation;
        public RotateAnimation rotateAnimation;
        protected TriggerControlledMovingPlatformComponent targetComponent;
        private IObserver<GameObject> turnEndObserver;

        protected TriggerControlledMovingPlatformState(TriggerControlledMovingPlatformComponent targetComponent)
        {
            this.targetComponent = targetComponent;
        }

        public abstract void OnAwake();

        public abstract void OnStart();

        public void OnUpdate()
        {
            if (moveAnimation != null) moveAnimation.Update();
            if (rotateAnimation != null) rotateAnimation.Update();

            if (moveAnimation == null && rotateAnimation == null && turnEndObserver != null)
            {
                turnEndObserver.OnNext(targetComponent.gameObject);
                turnEndObserver.OnCompleted();
                turnEndObserver = null;
            }
        }

        public abstract void OnTriggerPressed();

        public abstract void TriggerUnpressed();

        public IObservable<GameObject> OnTakeTurnEndsAction()
        {
            return Observable.Create<GameObject>(observer =>
            {
                turnEndObserver = observer;
                return Disposable.Empty;
            });
        }
    }
}