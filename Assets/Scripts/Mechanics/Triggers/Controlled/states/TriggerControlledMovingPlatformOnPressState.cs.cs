﻿using utils;
using Utils.Mechanics.Triggers.Controlled;

namespace Utils.Mechanics.Triggers.Platforms.states
{
    public class TriggerControlledMovingPlatformOnPressState : TriggerControlledMovingPlatformState
    {


        public TriggerControlledMovingPlatformOnPressState(
            TriggerControlledMovingPlatformComponent targetComponent) : base(targetComponent)
        {
        }

        public override void OnAwake()
        {
        }

        public override void OnStart()
        {
        }


        public override void OnTriggerPressed()
        {
            if (moveAnimation != null && !moveAnimation.IsFinishedAnimating) return;
            if (rotateAnimation != null && !rotateAnimation.IsFinishedAnimating) return;
            moveAnimation = targetComponent.CreateMoveToNextPointAnimation(() => { moveAnimation = null; });
            rotateAnimation = !targetComponent.ignoreRotation?
                targetComponent.CreateNextRotationAnimation(() => { rotateAnimation = null; }):
                null;
        }

        public override void TriggerUnpressed()
        {

        }
    }
}