﻿using UnityEngine;

public class TriggerComponent : MonoBehaviour
{
    public enum TriggerType
    {
        /// <summary>
        /// Single press will activate the platform every time someone steps on it.
        /// The trigger will be pushed in and right back out.
        /// It pushes
        /// </summary>
        SINGLE_PRESS,

        /// <summary>
        /// Toggle will be pressed once and stay pressed until pushed on again.
        /// The trigger will be pushed in , and will be stayed pushed untill someone pushes it again.
        /// </summary>
        TOGGLE,

        /// <summary>
        /// Push trigger will stay pushed in as long as something or someone pushes it constantly.
        /// Once the constant pressure is gone , trigger will unpress itself.
        /// </summary>
        PUSH,

        /// <summary>
        /// This type of trigger will be pushed once , and stay pressed for a certian
        /// amount of turns defined by autorelease value
        /// </summary>
        TIMED
    }

    [Tooltip(
        "SINGLE_PRESS - Single press will activate the platform every time someone steps on it.he trigger will be pushed in and right back out.\n" +
        "TOGGLE - Toggle will be pressed once and stay pressed until pushed on again.The trigger will be pushed in , and will be stayed pushed untill someone pushes it again.\n" +
        "PUSH - Push trigger will stay pushed in as long as something or someone pushes it constantly.Once the constant pressure is gone , trigger will unpress itself.\n" +
        "TIMED - This type of trigger will be only pushed once , and stay pushed forever , no matter what.")]
    public TriggerControlledObject[] controlledObjects;

    public TriggerType triggerType = TriggerType.SINGLE_PRESS;

    [Tooltip("Amount of turns before the switch will reset itself to unpressed position.\n" +
             "Works only with TIMED state")] [Range(1, 50)] public int autoreleaseTurns = 1;

    public bool pressed;

    public virtual bool Pressed
    {
        get { return pressed; }
        set
        {
            pressed = value;
            foreach (var controlled in controlledObjects)
            {
                if (pressed) controlled.TriggerPressed();
                else controlled.TriggerUnpressed();
            }
        }
    }
}