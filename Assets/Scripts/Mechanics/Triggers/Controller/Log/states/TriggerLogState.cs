﻿
namespace Utils.Mechanics.Triggers.Platforms.states
{
    public abstract class TriggerLogState
    {
        protected TriggerLogComponent targetComponent;

        protected TriggerLogState(TriggerLogComponent targetComponent)
        {
            this.targetComponent = targetComponent;
        }

        public abstract void OnCollisionEnter();

        public abstract void OnCollisionExit();

        public void OnUpdate()
        {
            //Do something ?
        }

        public abstract void OnStart();

        public abstract void OnTurnStart();
    }
}