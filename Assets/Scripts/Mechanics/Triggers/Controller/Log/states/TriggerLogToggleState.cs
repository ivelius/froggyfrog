﻿using System;
using UnityEngine;

namespace Utils.Mechanics.Triggers.Platforms.states
{
    public class TriggerLogToggleState : TriggerLogState
    {
        /// <summary>
        /// Amount of time that should pass between the triggers of the moveAnimation
        /// We need this value to make moveAnimation playing without interferance or bugs
        /// </summary>
        private const float GRACE_PERIOD = 0.5f; //seconds

//        private float lastTimeActivated;
        private bool isStateChangedThisTurn;

        public TriggerLogToggleState(TriggerLogComponent targetComponent) : base(targetComponent)
        {
        }

        public override void OnCollisionEnter()
        {
            if (isStateChangedThisTurn) return;
            //when moveAnimation is in progress we ignore the collider
//            if (IsGraceTimeElapsed()) return;

//            lastTimeActivated = Time.time;

            //toggle
            targetComponent.Pressed = !targetComponent.Pressed;
        }

        /// <summary>
        /// Decides wether there is enough time elapsed since the last trigger of the moveAnimation
        /// </summary>
        /// <returns></returns>
//        private bool IsGraceTimeElapsed()
//        {
//            return Time.time - lastTimeActivated < GRACE_PERIOD;
//        }
        public override void OnCollisionExit()
        {
        }


        public override void OnStart()
        {
        }

        public override void OnTurnStart()
        {
            isStateChangedThisTurn = false;
        }
    }
}