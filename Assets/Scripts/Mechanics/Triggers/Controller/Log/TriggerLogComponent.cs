﻿using System;
using UnityEngine;
using Utils.Mechanics.Triggers.Platforms.states;


public class TriggerLogComponent : TriggerComponent, TurnManager.ITurnStartActionTaker
{
    private TriggerLogState state;

    private void Awake()
    {
        ChangeState(triggerType);
    }

    private void ChangeState(TriggerType type)
    {
        state = TypeToState(type);
    }

    private TriggerLogState TypeToState(TriggerType type)
    {
        switch (type)
        {
//            case TriggerType.SINGLE_PRESS:
//                return new TriggerColumnSinglePressState(this);

            case TriggerType.TOGGLE:
                return new TriggerLogToggleState(this);

//            case TriggerType.PUSH:
//                return new TriggerColumnPushState(this);
//
//            case TriggerType.TIMED:
//                return new TriggerColumnTimedState(this);

            default:
                throw new Exception("Unknown trigger type : " + type);
        }
    }


    void Start()
    {
        //Notify enemy will participate to turn end action takers
//        TurnManager.INSTANCE.AddTurnStartActionTaker(this);
        FindObjectOfType<TurnManager>().AddTurnStartActionTaker(this);
        state.OnStart();
    }

    private void Update()
    {
        state.OnUpdate();
    }

    void OnCollisionEnter(Collision collision)
    {
        if(TurnManager.isUndoWasPressed) return;
        state.OnCollisionEnter();
    }

    private void OnCollisionExit(Collision other)
    {
        state.OnCollisionExit();
    }

    public void TakeTurnStartAction()
    {
        state.OnTurnStart();
    }
}