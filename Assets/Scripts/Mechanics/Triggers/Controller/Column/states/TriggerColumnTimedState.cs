﻿namespace Utils.Mechanics.Triggers.Platforms.states
{
    public class TriggerColumnTimedState : TriggerColumnState
    {
        private int turnsBeforeReset;

        public TriggerColumnTimedState(TriggerColumnComponent targetComponent) : base(targetComponent)
        {
        }

        public override void OnCollisionEnter()
        {
            if (turnsBeforeReset >= 0) return;

            //when moveAnimation is in progress we ignore the collider
            if (animation != null && !animation.IsFinishedAnimating) return;

            turnsBeforeReset = targetComponent.autoreleaseTurns;

            //play sinking moveAnimation
            animation = targetComponent.CreateSinkAnimation(() => animation = null);

            //toggle
            targetComponent.Pressed = true;
        }

        public override void OnCollisionExit()
        {
        }


        public override void OnStart()
        {
        }

        public override void OnTurnStart()
        {
            turnsBeforeReset--;

            //we will wait with the floating moveAnimation until the amount of turns runs out
            if (turnsBeforeReset > 0) return;
            animation = targetComponent.CreateFloatAnimation(() => animation = null);
            targetComponent.Pressed = false;
        }

    }
}