﻿using utils;
using UnityEngine;

namespace Utils.Mechanics.Triggers.Platforms.states
{
    public class TriggerColumnSinglePressState : TriggerColumnState
    {

        public TriggerColumnSinglePressState(TriggerColumnComponent targetComponent) : base(targetComponent)
        {
        }

        public override void OnCollisionEnter()
        {
            //when moveAnimation is in progress we ignore the collider
            if (animation != null && animation.IsFinishedAnimating) return;
            targetComponent.Pressed = true;

            //We simply do press and unpress immideatley
            animation = targetComponent.CreateSinkAnimation(() =>
                animation = targetComponent.CreateFloatAnimation(
                    () =>
                    {
                        //We mark the trigger as unpressed
                        targetComponent.Pressed = false;
                        //moveAnimation needs to be cleaned
                        animation = null;
                    }
                ));
        }

        public override void OnCollisionExit()
        {
            //Here we do nothing
        }


        public override void OnStart()
        {
            //We unpress the button if it is pressed already
        }

        public override void OnTurnStart()
        {
        }
    }
}