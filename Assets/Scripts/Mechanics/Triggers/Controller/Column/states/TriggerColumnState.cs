﻿using utils;

namespace Utils.Mechanics.Triggers.Platforms.states
{
    public abstract class TriggerColumnState
    {
        protected TriggerColumnComponent targetComponent;
        protected MoveAnimation animation;

        protected TriggerColumnState(TriggerColumnComponent targetComponent)
        {
            this.targetComponent = targetComponent;
        }

        public abstract void OnCollisionEnter();

        public abstract void OnCollisionExit();

        public void OnUpdate()
        {
            if (animation != null) animation.Update();
        }

        public abstract void OnStart();

        public abstract void OnTurnStart();
    }
}