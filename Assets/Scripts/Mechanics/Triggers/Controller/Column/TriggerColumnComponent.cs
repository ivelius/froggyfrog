﻿using System;
using utils;
using UniRx;
using UnityEngine;
using Utils;
using Utils.Mechanics.Triggers.Platforms.states;

public class TriggerColumnComponent : TriggerComponent, TurnManager.ITurnStartActionTaker,
    UndoComponent.IUndoStateCreator, TurnManager.ITurnEndActionTaker
{
    private const float SINKING_DEPTH = 0.1f;

    public float animateSpeed = 0.5f;
    private TriggerColumnState state;
    private float initialYPosition;
    public SignComponent triggerSignComponent;
    public SignComponent signComponent;
    [Range(0, 6)] public int signNumber;
    private bool isRestoredFromUndo;


    #region UndoRegion

    internal class UndoState : UndoComponent.IUndoState
    {
        internal readonly Vector3 position;
        internal readonly Quaternion rotation;
        internal readonly TriggerColumnState state;

        public UndoState(Vector3 position, Quaternion rotation, TriggerColumnState state)
        {
            this.position = position;
            this.rotation = rotation;
            this.state = state;
        }
    }

    public UndoComponent.IUndoState createUndoState()
    {
        return new UndoState(FrogUtils.CloneVector3(transform.position),
            FrogUtils.CloneQuaternion(transform.rotation), state);
    }


    public void undoState(UndoComponent.IUndoState state)
    {
        var undoState = state as UndoState;
        transform.position = undoState.position;
        transform.rotation = undoState.rotation;
        this.state = undoState.state;
        isRestoredFromUndo = true;
    }

    #endregion

    private void Awake()
    {
        initialYPosition = transform.position.y;
        ChangeState(triggerType);
    }

    private void ChangeState(TriggerType type)
    {
        state = TypeToState(type);
    }

    private void OnDrawGizmos()
    {
        if (triggerSignComponent == null) return;
        triggerSignComponent.selectedSymbol = TypeToSignIndex(triggerType);
        signComponent.selectedSymbol = signNumber;

        foreach (var co in controlledObjects)
            co.GetComponentInChildren<SignComponent>().selectedSymbol = signNumber;
    }

    private int TypeToSignIndex(TriggerType type)
    {
        switch (type)
        {
            case TriggerType.SINGLE_PRESS:
                return 0;

            case TriggerType.TOGGLE:
                return 1;

            case TriggerType.PUSH:
                return 2;

            case TriggerType.TIMED:
                return 2;

            default:
                throw new Exception("Unknown trigger type : " + type);
        }
    }

    private TriggerColumnState TypeToState(TriggerType type)
    {
        switch (type)
        {
            case TriggerType.SINGLE_PRESS:
                return new TriggerColumnSinglePressState(this);

            case TriggerType.TOGGLE:
                return new TriggerColumnToggleState(this);

            case TriggerType.PUSH:
                return new TriggerColumnPushState(this);

            case TriggerType.TIMED:
                return new TriggerColumnTimedState(this);

            default:
                throw new Exception("Unknown trigger type : " + type);
        }
    }


    void Start()
    {
        //Notify enemy will participate to turn end action takers
//        TurnManager.INSTANCE.AddTurnStartActionTaker(this);
        FindObjectOfType<TurnManager>().AddTurnStartActionTaker(this);
        FindObjectOfType<TurnManager>().AddTurnEndActionTaker(this);
        state.OnStart();
    }

    private void Update()
    {
        state.OnUpdate();
    }


    void OnCollisionEnter(Collision collision)
    {
        if (isRestoredFromUndo) return;
        state.OnCollisionEnter();
    }

    private void OnCollisionExit(Collision other)
    {
        state.OnCollisionExit();
    }

    public MoveAnimation CreateFloatAnimation(Action endAction = null)
    {
        var dest = FrogUtils.CloneVector3(transform.position);
        dest.y = initialYPosition;
        return new MoveAnimation(transform, animateSpeed, dest, endAction);
    }

    public MoveAnimation CreateSinkAnimation(Action endAction = null)
    {
        var dest = FrogUtils.CloneVector3(transform.position);
        dest.y = initialYPosition - SINKING_DEPTH;
        return new MoveAnimation(transform, animateSpeed, dest, endAction);
    }

    public void TakeTurnStartAction()
    {
        state.OnTurnStart();
    }

    public UniRx.IObservable<GameObject> TakeTurnEndsAction()
    {
        return Observable.Empty<GameObject>();
    }

    public int GetTurnEndPriority()
    {
        return 0;
    }

    public void TurnWillEnd()
    {
        isRestoredFromUndo = false;
    }
}