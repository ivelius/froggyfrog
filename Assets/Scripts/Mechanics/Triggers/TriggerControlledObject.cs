﻿using UnityEngine;

public abstract class TriggerControlledObject : MonoBehaviour
{
    public enum TriggerReactionType
    {
        /// <summary>
        /// Object will only react when trigger is pressed.
        /// </summary>
        ON_PRESS,

        /// <summary>
        /// Object will only react when trigger is unpressed.
        /// </summary>
        ON_UNPRESS,

        /// <summary>
        /// Object will only react when trigger is pressed or unpressed.
        /// </summary>
        ON_ALL
    }

    public TriggerReactionType reactionType;

    public abstract void TriggerPressed();

    public abstract void TriggerUnpressed();
}