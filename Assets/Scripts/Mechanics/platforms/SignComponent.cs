﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignComponent : MonoBehaviour
{
    public Material[] symbolMaterials;
    public int selectedSymbol;
    private int previousSelectedSymbol;
    private MeshRenderer meshRenderer;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }


    private void OnDrawGizmos()
    {
        if (meshRenderer == null) meshRenderer = GetComponent<MeshRenderer>();
        if (previousSelectedSymbol == selectedSymbol) return;
        if (selectedSymbol > symbolMaterials.Length || symbolMaterials.Length == 0) return;

        previousSelectedSymbol = selectedSymbol;
        meshRenderer.material = symbolMaterials[selectedSymbol];
    }
}