﻿using utils;
using UniRx;
using UnityEngine;
using Utils;

public class BreakingColumnComponent : MonoBehaviour, TurnManager.ITurnEndActionTaker,
    TurnManager.ITurnStartActionTaker, UndoComponent.IUndoStateCreator
{
    internal enum STATE
    {
        WHOLE,
        CRACKED,
        BROKEN
    }

    private STATE state = STATE.WHOLE;

    private MoveAnimation fallDownAnimation;

    private bool isAlreadyActedThisTurn;
    private IObserver<GameObject> turnManagerObserver;
    private IObservable<GameObject> collapsingObservable;
    private bool isRestoredFromUndo;

    internal class UndoState : UndoComponent.IUndoState
    {
        internal readonly Vector3 position;
        internal readonly Quaternion rotation;
        internal readonly STATE state;

        public UndoState(Vector3 position, Quaternion rotation, STATE state)
        {
            this.position = position;
            this.rotation = rotation;
            this.state = state;
        }
    }

    private void Awake()
    {
    }


    // Use this for initialization
    void Start()
    {
        FindObjectOfType<TurnManager>().AddTurnStartActionTaker(this);
        FindObjectOfType<TurnManager>().AddTurnEndActionTaker(this);
    }

    // Update is called once per frame
    void Update()
    {
        if (fallDownAnimation != null)
        {
            fallDownAnimation.Update();
            return;
        }

        if (state == STATE.BROKEN && fallDownAnimation == null && turnManagerObserver != null)
        {
            turnManagerObserver.OnNext(gameObject);
            turnManagerObserver.OnCompleted();
            turnManagerObserver = null;
            collapsingObservable = null;
        }
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (isAlreadyActedThisTurn || isRestoredFromUndo) return;
        isAlreadyActedThisTurn = true;

        if (state == STATE.WHOLE)
        {
            GetComponent<Animator>().SetTrigger("crack");
            state = STATE.CRACKED;
            return;
        }
        if (state == STATE.CRACKED)
        {
            GetComponent<Animator>().SetTrigger("break");
            state = STATE.BROKEN;
            var dest = FrogUtils.CloneVector3(transform.position);
            dest.y = -2;
            fallDownAnimation = new MoveAnimation(transform, 3, dest, () => fallDownAnimation = null);
            collapsingObservable = CreateObservable();
            return;
        }
    }

    public void TakeTurnStartAction()
    {
        isAlreadyActedThisTurn = false;
    }

    public IObservable<GameObject> TakeTurnEndsAction()
    {
        isAlreadyActedThisTurn = false;
        return collapsingObservable != null ? collapsingObservable : Observable.Empty<GameObject>();
    }

    public int GetTurnEndPriority()
    {
        return 10;
    }

    private IObservable<GameObject> CreateObservable()
    {
        return Observable.Create<GameObject>(observer =>
        {
            turnManagerObserver = observer;
            return Disposable.Empty;
        });
    }

    public UndoComponent.IUndoState createUndoState()
    {
        return new UndoState(FrogUtils.CloneVector3(transform.position),
            FrogUtils.CloneQuaternion(transform.rotation), this.state);
    }

    public void undoState(UndoComponent.IUndoState state)
    {
        var undoState = state as UndoState;
        transform.position = undoState.position;
        transform.rotation = undoState.rotation;

//        this.isAlreadyActedThisTurn = true;
        isRestoredFromUndo = true;

        if (this.state == STATE.CRACKED && undoState.state == STATE.WHOLE)
            GetComponent<Animator>().SetTrigger("normal");
        else if (this.state == STATE.BROKEN && undoState.state == STATE.CRACKED)
            GetComponent<Animator>().SetTrigger("crack");
        else
        {
            //do nothing
        }

        this.state = undoState.state;
    }

    public void TurnWillEnd()
    {
        isRestoredFromUndo = false;
    }
}