﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class DraggedObjectComp : MonoBehaviour
{
    private Vector3 initialPosition;
    private Quaternion initialRotation;

    public Vector3 InitialPosition
    {
        get { return initialPosition; }
    }

    public Quaternion InitialRotation
    {
        get { return initialRotation; }
    }


    private void Awake()
    {
        initialPosition = FrogUtils.CloneVector3(transform.position);
        initialRotation = FrogUtils.CloneQuaternion(transform.rotation);
    }



    // Update is called once per frame
    void Update()
    {
    }
}