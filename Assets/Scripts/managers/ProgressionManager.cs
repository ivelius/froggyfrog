﻿using System;
using UnityEngine;

namespace stats
{
    public class ProgressionManager
    {
        public const string STARS_FOR_LEVEL_PREF_KEY = "STARS_FOR_LEVEL_PREF_KEY";


        public enum Level
        {
            _1,
            _2,
            _3,
            _4,
            _5,
            _6,
            _7,
            _8,
            _9,
            _10,
            _11,
            _12,
            _13,
            _14,
            _15
        }


        static ProgressionManager()
        {
            init();
        }

        public static void init()
        {
            foreach (Level level in Enum.GetValues(typeof(Level)))
            {
                if (!PlayerPrefs.HasKey(STARS_FOR_LEVEL_PREF_KEY + level))
                    PlayerPrefs.SetInt(STARS_FOR_LEVEL_PREF_KEY + level, 0);
            }
        }


        public static void SetStarsForLevel(Level level, int starsAmount)
        {
            PlayerPrefs.SetInt(STARS_FOR_LEVEL_PREF_KEY + level, starsAmount);
        }

        public static int GetStarsForLevel(Level level)
        {
            return PlayerPrefs.GetInt(STARS_FOR_LEVEL_PREF_KEY + level);
        }
    }
}