﻿using System;
using UnityEngine;

namespace stats
{
    public class StatsManager
    {
        public const string GOLD_STAT_PREF_KEY = "gold_stat";
        public const string LIVES_STAT_PREF_KEY = "lives_stat";
        public const string BUTTERFLIES_STAT_PREF_KEY = "butterflies_stat";
        public const string ENERGY_STAT_PREF_KEY = "energy_stat";

        public enum Type
        {
            LIVES,
            BUTTERFLIES,
            GOLD,
            ENERGY
        }
        
        static StatsManager()
        {
            init();
        }

        public static string GetStringStatFromPreferences(Type statType)
        {
            return PlayerPrefs.GetString(GetPrefKeyForStatType(statType));
        }

        public static float GetFloatStatFromPreferences(Type statType)
        {
            return PlayerPrefs.GetFloat(GetPrefKeyForStatType(statType));
        }

        public static int GetIntStatFromPreferences(Type statType)
        {
            return PlayerPrefs.GetInt(GetPrefKeyForStatType(statType));
        }

        private static string GetPrefKeyForStatType(Type statType)
        {
            switch (statType)
            {
                case Type.LIVES:
                    return LIVES_STAT_PREF_KEY;
                case Type.BUTTERFLIES:
                    return BUTTERFLIES_STAT_PREF_KEY;
                case Type.GOLD:
                    return GOLD_STAT_PREF_KEY;
                case Type.ENERGY:
                    return ENERGY_STAT_PREF_KEY;
                default:
                    throw new Exception("Unknown Type");
            }
        }

        public static void StoreFloatStatInPreferences(Type statType, float value)
        {
            PlayerPrefs.SetFloat(GetPrefKeyForStatType(statType), value);
        }

        public static void StoreIntStatInPreferences(Type statType, int value)
        {
            PlayerPrefs.SetInt(GetPrefKeyForStatType(statType), value);
        }

        public static void init()
        {
            if (!PlayerPrefs.HasKey(GOLD_STAT_PREF_KEY))
                PlayerPrefs.SetInt(GOLD_STAT_PREF_KEY, 100);
            if (!PlayerPrefs.HasKey(LIVES_STAT_PREF_KEY))
                PlayerPrefs.SetInt(LIVES_STAT_PREF_KEY, 5);
            if (!PlayerPrefs.HasKey(BUTTERFLIES_STAT_PREF_KEY))
                PlayerPrefs.SetInt(BUTTERFLIES_STAT_PREF_KEY, 0);
            if (!PlayerPrefs.HasKey(ENERGY_STAT_PREF_KEY))
                PlayerPrefs.SetInt(ENERGY_STAT_PREF_KEY, 10);
        }

        public static void reset()
        {
            PlayerPrefs.DeleteAll();
        }
    }
}