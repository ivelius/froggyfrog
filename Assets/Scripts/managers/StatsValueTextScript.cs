﻿using System.Collections;
using System.Collections.Generic;
using stats;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class StatsValueTextScript : MonoBehaviour
{
    public StatsManager.Type stat;

    // Use this for initialization
    void Start()
    {
        UpdateStats();
    }

    public void UpdateStats()
    {
        StatsManager.init();
        var statAmount = StatsManager.GetIntStatFromPreferences(stat);
        GetComponent<Text>().text = "" + statAmount;
    }

    // Update is called once per frame
    void Update()
    {
    }
}