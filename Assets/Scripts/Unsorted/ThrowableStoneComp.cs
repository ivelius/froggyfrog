﻿using UnityEngine;

public class ThrowableStoneComp : MonoBehaviour
{
    private bool autoDestroy;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
//        if (autoDestroy)
//            transform.position = new Vector3(
//                transform.position.x,
//                transform.position.y + 0.02f,
//                transform.position.z
//            );
    }

    private void OnCollisionEnter(Collision other)
    {
        if (autoDestroy) return;
        autoDestroy = true;
//        Invoke("AutoDisable", 0.7f);
//        Invoke("AutoDestroy", 5f);
        Invoke("AutoRemoveRigidBody", 0.3f);

//        Destroy(GetComponent<BoxCollider>());
    }

    private void AutoRemoveRigidBody()
    {
        Destroy(GetComponent<Rigidbody>());
        Destroy(GetComponent<BoxCollider>());
    }

    private void AutoDisable()
    {
        gameObject.SetActive(false);
    }

    private void AutoDestroy()
    {
        Destroy(gameObject);
    }
}