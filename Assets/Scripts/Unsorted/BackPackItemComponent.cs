﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Utils.Items;

/// <summary>
/// This is a UI representation of a backpack item
/// </summary>
[RequireComponent(typeof(MyUIToggle))]
public class BackPackItemComponent : MonoBehaviour, BackPackComponent.IBackPackListener
{
    private MyUIToggle stoneToggle;
    public Text stoneCountText;
    public BackPackComponent backpack;

    private void Awake()
    {
        stoneToggle = GetComponent<MyUIToggle>();
    }

    private void OnEnable()
    {
        UpdateStoneButton(backpack);
        backpack.AddListener(this);
    }

    private void OnDisable()
    {
        backpack.RemoveListener(this);
    }

    // Use this for initialization
    void Start()
    {
    }

    private void UpdateStoneButton(BackPackComponent backpack)
    {
        var items = backpack.GetItemsOfType<StoneBooster>();

        if (!items.Any())
        {
            stoneToggle.isOn = false;
            stoneCountText.text = "0";
        }

        stoneToggle.IsInteractive = items.Any();
        stoneCountText.text = items.Count().ToString();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void OnCollectableAddedToBackpack(ICollectableItem item, BackPackComponent backpack)
    {
        UpdateStoneButton(backpack);
    }

    public void OnCollectableRemovedFromBackpack(ICollectableItem item, BackPackComponent backpack)
    {
        stoneToggle.IsOn = false;
        UpdateStoneButton(backpack);
    }
}