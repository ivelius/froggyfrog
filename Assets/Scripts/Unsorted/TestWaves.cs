﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestWaves : MonoBehaviour
{
//    public float scale = 0.1f;
//    public float speed = 1.0f;
//    public float noiseStrength = 1f;
//    public float noiseWalk = 1f;
//
//    private Vector3[] baseHeight;
//
//    void Update () {
//        Mesh mesh = GetComponent<MeshFilter>().mesh;
//
//        if (baseHeight == null)
//            baseHeight = mesh.vertices;
//
//        Vector3[] vertices = new Vector3[baseHeight.Length];
//        for (int i=0;i<vertices.Length;i++)
//        {
//            Vector3 vertex = baseHeight[i];
//            vertex.y += Mathf.Sin(Time.time * speed+ baseHeight[i].x + baseHeight[i].y + baseHeight[i].z) * scale;
//            vertex.y += Mathf.PerlinNoise(baseHeight[i].x + noiseWalk, baseHeight[i].y + Mathf.Sin(Time.time * 0.1f)    ) * noiseStrength;
//            vertices[i] = vertex;
//        }
//        mesh.vertices = vertices;
//        mesh.RecalculateNormals();
//        GetComponent<MeshCollider>().sharedMesh = mesh;
//    }

    Vector3 waveSource1 = new Vector3(2.0f, 0.0f, 2.0f);
    public float freq1 = 0.1f;
    public float amp1 = 0.01f;
    public float waveLength1 = 0.05f;
    Mesh mesh;
    Vector3[] vertices;

    // Use this for initialization
    void Start()
    {
        MeshFilter mf = GetComponent<MeshFilter>();
        if (mf == null)
        {
            //Debug.Log("No mesh filter");
            return;
        }
        mesh = mf.mesh;
        vertices = mesh.vertices;
    }

    // Update is called once per frame
    void Update()
    {
        CalcWave();
    }

    void CalcWave()
    {
        for (int i = 0; i < vertices.Length; i++)
        {
            Vector3 v = vertices[i];
            v.y = 0.0f;
            float dist = Vector3.Distance(v, waveSource1);
            dist = (dist % waveLength1) / waveLength1;
            v.y = amp1 * Mathf.Sin(Time.time * Mathf.PI * 2.0f * freq1
                                   + (Mathf.PI * 2.0f * dist));
            vertices[i] = v;
        }
        mesh.vertices = vertices;
        GetComponent<MeshCollider>().sharedMesh = mesh;
    }
}