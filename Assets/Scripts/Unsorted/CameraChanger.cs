﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChanger : MonoBehaviour
{
    public Camera topViewCamera;
    public Camera sideViewCamera;

    // Use this for initialization
    void Start()
    {
        topViewCamera.enabled = false;
        sideViewCamera.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void ChangeCameraAngle()
    {
        topViewCamera.enabled = !topViewCamera.enabled;
        sideViewCamera.enabled = !sideViewCamera.enabled;
    }
}