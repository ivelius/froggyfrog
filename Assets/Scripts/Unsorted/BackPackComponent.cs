﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utils.Items;

/// <summary>
/// Backpack manages all collected items
/// </summary>
public class BackPackComponent : MonoBehaviour
{
    private readonly List<ICollectableItem> collectableItems = new List<ICollectableItem>();
    private List<IBackPackListener> listeners = new List<IBackPackListener>();

    public interface IBackPackListener
    {
        void OnCollectableAddedToBackpack(ICollectableItem item, BackPackComponent backpack);
        void OnCollectableRemovedFromBackpack(ICollectableItem item, BackPackComponent backpack);
    }

    private void Awake()
    {
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void AddCollectableItem(ICollectableItem item)
    {
        collectableItems.Add(item);
        listeners.ForEach(listener => listener.OnCollectableAddedToBackpack(item, this));
    }

    public IEnumerable<ICollectableItem> GetItemsOfType<T>()
    {
        return collectableItems.Where(item => item.GetType() == typeof(T));
    }

    public void RemoveItem(ICollectableItem item)
    {
        collectableItems.Remove(item);
        listeners.ForEach(listener => listener.OnCollectableRemovedFromBackpack(item, this));
    }

    public void AddListener(IBackPackListener listener)
    {
        listeners.Add(listener);
    }

    public void RemoveListener(IBackPackListener listener)
    {
        listeners.Remove(listener);
    }
}