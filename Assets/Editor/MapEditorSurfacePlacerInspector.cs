﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MapEditorSurfacePlacerComp))]
public class MapEditorSurfacePlacerInspector : UnityEditor.Editor
{
    internal class SurfaceData
    {
        internal string displayName;
        internal Mesh mesh;
        internal string prefabName;

        public SurfaceData(string displayName, Mesh mesh, string prefabName)
        {
            this.displayName = displayName;
            this.mesh = mesh;
            this.prefabName = prefabName;
        }
    }

    private List<SurfaceData> surfaceDataList;
    private SurfaceData selectedSurfaceData;

    private void OnEnable()
    {
        // Init here
        surfaceDataList = CreateSurfaceDataList();
        selectedSurfaceData = surfaceDataList.First();
    }

    private List<SurfaceData> CreateSurfaceDataList()
    {
        //TODO : fill this list from editor ???
        var list = new List<SurfaceData>();
        list.Add(new SurfaceData("None", null, null));
        list.Add(new SurfaceData("Lilly", LoadMeshFromPrefab("LilyPrefab"), "LilyPrefab"));
//        list.Add(new SurfaceData("Log", LoadMeshFromPrefab("LogPrefab"), "LogPrefab"));
        list.Add(new SurfaceData("TriggerColumn", LoadMeshFromPrefab("ColumnTriggerPrefab"), "ColumnTriggerPrefab"));
        list.Add(new SurfaceData("Column", LoadMeshFromPrefab("ColumnPrefab"), "ColumnPrefab"));
        list.Add(new SurfaceData("BrokenColumn", LoadMeshFromPrefab("BrokenColumnPrefab"), "BrokenColumnPrefab"));
//        list.Add(new SurfaceData("HalfLog", LoadMeshFromPrefab("HalfLogPrefab"), "HalfLogPrefab"));
        list.Add(new SurfaceData("Raft", LoadMeshFromPrefab("MovingRiftPrefab"), "MovingRiftPrefab"));
        list.Add(new SurfaceData("Turtle", LoadMeshFromPrefab("TurtlePrefab"), "TurtlePrefab"));

        return list;
    }

    private static Mesh LoadMeshFromPrefab(string prefabname)
    {
        //some have a mesh filter some others have SkinnedMeshRenderer , we are trying to load either of them
        var prefab = LoadPrefab(prefabname);
        var comp = prefab.GetComponentInChildren<MeshFilter>();
        if (comp != null) return comp.sharedMesh;

        var comp2 = prefab.GetComponentInChildren<SkinnedMeshRenderer>();
        return comp2.sharedMesh;
    }

    private static GameObject LoadPrefab(string prefabname)
    {
        return (GameObject) Resources.Load("Prefabs/" + prefabname);
    }

    private void OnDisable()
    {
        //TODO : Cleanup here
    }


//    public override void OnInspectorGUI()
//    {
//        DrawDefaultInspector();
//    }

    private void OnSceneGUI()
    {
        DrawSidePanel();
        HandlePlacement();
        SceneView.RepaintAll();
    }

    private void HandlePlacement()
    {
        if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
        {
//            //Debug.Log("Left-Click Down");
        }
        if (Event.current.type == EventType.MouseUp && Event.current.button == 0)
        {
//            //Debug.Log("Left-Click Up");
            if (selectedSurfaceData.prefabName != null)
                getTargetComponent().PlaceObjectOnGrid(LoadPrefab(selectedSurfaceData.prefabName));
        }

        if (Event.current.type == EventType.MouseDown && Event.current.button == 1)
        {
//            //Debug.Log("Right-Click Down");
        }
        if (Event.current.type == EventType.MouseUp && Event.current.button == 1)
        {
//            //Debug.Log("Right-Click Up");
            getTargetComponent().DeleteObjectOnCurrentPosition();
        }
    }

    private void DrawSidePanel()
    {
        Handles.BeginGUI();
        {
            GUILayout.BeginVertical();
            {
                GUILayout.BeginArea(new Rect(20, 20, 60, 500));
                {
                    GUILayout.Label("Surface");
                    foreach (var data in surfaceDataList)
                    {
                        if (GUILayout.Button(data.displayName,
                            GUILayout.MinHeight(50), GUILayout.MinWidth(250)))
                        {
                            getTargetComponent().SelectedMesh = data.mesh;
                            selectedSurfaceData = data;
                        }
                    }
                }
                GUILayout.EndArea();
            }
            GUILayout.EndVertical();
        }
        Handles.EndGUI();
    }


    /// <summary>
    /// Gets target as inspected component type
    /// </summary>
    /// <returns></returns>
    private MapEditorSurfacePlacerComp getTargetComponent()
    {
        return target as MapEditorSurfacePlacerComp;
    }
}