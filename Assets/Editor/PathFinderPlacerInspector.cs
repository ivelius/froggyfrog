﻿using System.Linq;
using Editor;
using UnityEditor;
using UnityEngine;
using Utils;

[CustomEditor(typeof(PathFinderComp))]
public class PathFinderInspector : UnityEditor.Editor
{
    private readonly GUIContent jumpRadiusGuiContent =
        new GUIContent("Jump Radius", "Defines the distance of the jump to all directions");

    private void OnEnable()
    {
        // TODO : Init here
    }


    private void OnDisable()
    {
        //TODO : Cleanup here
    }

    public override void OnInspectorGUI()
    {
        DrawInspectorJumpDistanceSlider();
        DrawDefaultInspector();
    }

    private void DrawInspectorJumpDistanceSlider()
    {
//        FrogInspectorUtils.ScaleValueFloatRange(ref getTargetComponent().jumpRadius,
//            jumpRadiusGuiContent, "Change jump radius",
//            PathFinderComp.MIN_JUMP_RADIUS, PathFinderComp.MAX_JUMP_RADIUS,
//            getTargetComponent(), () =>
//            {
//                //TODO : do something when radius updated ? ReDraw jump route ?
//            });
    }

    private void OnSceneGUI()
    {
        DrawJumpDistanceSphere();
        DrawJumpRoutes();
        SceneView.RepaintAll();
    }

    private void DrawJumpRoutes()
    {
        var sorroundSurfaces = getTargetComponent().FindReachableJumpPoints();

        var startPos = getTargetComponent().transform.position;
        var jmpheightOffset = getTargetComponent().GetComponent<JumperComponent>().jumpHeight;
        foreach (var jumpComp in sorroundSurfaces)
        {
            Handles.color = Color.blue;
            var endPos = jumpComp.getJumpTouchPoint();
            var middlePos = Vector3.Lerp(startPos, endPos, 0.5f);
            middlePos.y += jmpheightOffset;
            Handles.DrawDottedLine(startPos, middlePos, 5f);
            Handles.DrawDottedLine(middlePos, endPos, 5f);
        }
    }

    private void DrawJumpDistanceSphere()
    {
        EditorGUI.BeginChangeCheck();
        var newRadius = Mathf.Clamp(Handles.RadiusHandle(Quaternion.identity,
                getTargetComponent().transform.position, getTargetComponent().jumpRadius),
            PathFinderComp.MIN_JUMP_RADIUS, PathFinderComp.MAX_JUMP_RADIUS);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(target, "Change Jump Radius");
            getTargetComponent().jumpRadius =  newRadius;
        }
    }


    /// <summary>
    /// Gets target as inspected component type
    /// </summary>
    /// <returns></returns>
    private PathFinderComp getTargetComponent()
    {
        return target as PathFinderComp;
    }
}