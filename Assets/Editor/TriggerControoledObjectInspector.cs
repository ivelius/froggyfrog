﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Utils.Mechanics.Triggers.Controlled;

[CustomEditor(typeof(TriggerControlledMovingPlatformComponent))]
public class TriggerControlledMovingPlatformInspector : UnityEditor.Editor
{
   

    void OnSceneGUI()
    {
        float dashSize = 4.0f;
        Handles.color = Color.red;
        Handles.DrawDottedLine(getTargetComponent().transform.position, getTargetComponent().destination,
            dashSize);
        Handles.color = Color.white;
    }


    private void OnEnable()
    {
        // Init here
    }


    private void OnDisable()
    {
        //TODO : Cleanup here
    }


//    public override void OnInspectorGUI()
//    {
//        DrawDefaultInspector();
//    }


    /// <summary>
    /// Gets target as inspected component type
    /// </summary>
    /// <returns></returns>
    private TriggerControlledMovingPlatformComponent getTargetComponent()
    {
        return target as TriggerControlledMovingPlatformComponent;
    }
}