﻿using System;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Editor
{
    public class FrogInspectorUtils
    {
        public static void ScaleValueRange(ref int monitoredValue, GUIContent guiContent,
            string undoMsg, int minValue, int maxValue, Object targetComp, Action action)
        {
            EditorGUI.BeginChangeCheck();

            var newVal = (int) EditorGUILayout.Slider(guiContent,
                monitoredValue, minValue, maxValue);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(targetComp, undoMsg);
                monitoredValue = newVal;
                action();
                EditorUtility.SetDirty(targetComp);
            }
        }

        public static void ScaleValueFloatRange(ref float monitoredValue, GUIContent guiContent,
            string undoMsg, float minValue, float maxValue, Object targetComp, Action action)
        {
            EditorGUI.BeginChangeCheck();

            var newVal = EditorGUILayout.Slider(guiContent,
                monitoredValue, minValue, maxValue);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(targetComp, undoMsg);
                monitoredValue = newVal;
                action();
                EditorUtility.SetDirty(targetComp);
            }
        }
    }
}