﻿using System;
using Editor;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MapEditorGridComp))]
public class MapEditorInspector : UnityEditor.Editor
{
    //Grid dimentions will be clamped to those dimentions
    private const int MAX_GRID_SIZE = 10;

    private const int MIN_GRID_SIZE = 1;

    private readonly GUIContent gridWidthContent =
        new GUIContent("Grid Width", "Amount Of squares that grid has in X direction");

    private readonly GUIContent gridLengthContent =
        new GUIContent("Grid Length", "Amount Of squares that grid has in Z direction");

    private const float Y_POSITION = 0;


    private void OnEnable()
    {
        //TODO : Init here
        Subscribe();
    }

    private void OnDisable()
    {
        //TODO : Cleanup here
        Unsubscribe();
    }

    private void Subscribe()
    {
        //we subscribing the collider update function of the grid so the collider could be recalculated
        //When undo of grid change is performed
        Undo.undoRedoPerformed += getTargetComponent().UpdateColliderBoundries;
    }

    private void Unsubscribe()
    {
        Undo.undoRedoPerformed -= getTargetComponent().UpdateColliderBoundries;
    }


    public override void OnInspectorGUI()
    {
        DrawGridSize();
        DrawSnapGridToWorldGrid();
    }

    private void DrawGridSize()
    {
        Action action = () => getTargetComponent().UpdateColliderBoundries();
        FrogInspectorUtils.ScaleValueRange(ref getTargetComponent().gridWidth, gridWidthContent, "Change grid width",
            MIN_GRID_SIZE, MAX_GRID_SIZE, getTargetComponent(), action);
        FrogInspectorUtils.ScaleValueRange(ref getTargetComponent().gridLength, gridLengthContent, "Change grid legth",
            MIN_GRID_SIZE, MAX_GRID_SIZE, getTargetComponent(), action);
    }


    private void OnSceneGUI()
    {
        CalculateMousePointer();
        PreventShiftingFocus();

        SceneView.RepaintAll();
    }

    private void DrawSnapGridToWorldGrid()
    {
        if (GUILayout.Button("Snap Grid"))
        {
            var pos = getTargetComponent().gameObject.transform.position;
            getTargetComponent().gameObject.transform.position = new Vector3(
                Mathf.Round(pos.x),
                Y_POSITION,
                Mathf.Round(pos.z));
            getTargetComponent().UpdateColliderBoundries();
        }
    }

    /// <summary>
    /// When we are hovering over grid , we would like to use click events
    /// To interact with a grid , therefore we disable default mouse event handling
    /// while we are hovering the grid.
    /// </summary>
    private void PreventShiftingFocus()
    {
        if (getTargetComponent().isMouseHoveringGrid &&
            Event.current.type == EventType.Layout)
            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
    }


    /// <summary>
    /// Calculates the position of the mouse that intersects with the grid.
    /// </summary>
    private void CalculateMousePointer()
    {
        var ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        RaycastHit hit;
        if (!Physics.Raycast(ray.origin, ray.direction, out hit, Mathf.Infinity))
        {
            getTargetComponent().SetMouseHover(false);
            return;
        }
        var placePos = hit.point;
//            var placeRot = Quaternion.FromToRotation( Vector3.up, hit.normal );

        //we constrain the position to fit the grid dimentions
        //we also snap the position to grid units witch are whole integer numbrers
        var xMinBorder = getTargetComponent().gameObject.transform.position.x;
        var zMinBorder = getTargetComponent().gameObject.transform.position.z;

        var clampedMouseHitPosition = new Vector3(
            //X
            (float) Math.Round(Mathf.Clamp(placePos.x, xMinBorder,
                xMinBorder + getTargetComponent().gridWidth - 1)),
            //Y
            Y_POSITION,
            //Z
            (float) Math.Round(Mathf.Clamp(placePos.z, zMinBorder,
                zMinBorder + getTargetComponent().gridLength - 1)));

        //finaly we set the position on the component
        getTargetComponent().SetMouseHoverPosition(clampedMouseHitPosition);
    }


    /// <summary>
    /// Gets target as inspected component type
    /// </summary>
    /// <returns></returns>
    private MapEditorGridComp getTargetComponent()
    {
        return target as MapEditorGridComp;
    }
}