﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TriggerColumnComponent))]
public class TriggerColumnInspector : UnityEditor.Editor
{
    float dashSize = 4.0f;

    void OnSceneGUI()
    {
        foreach (var controlledObject in getTargetComponent().controlledObjects)
        {
            Handles.DrawDottedLine(getTargetComponent().transform.position, controlledObject.transform.position,
                dashSize);
        }
    }


    private void OnEnable()
    {
        // Init here
    }


    private void OnDisable()
    {
        //TODO : Cleanup here
    }


//    public override void OnInspectorGUI()
//    {
//        DrawDefaultInspector();
//    }


    /// <summary>
    /// Gets target as inspected component type
    /// </summary>
    /// <returns></returns>
    private TriggerColumnComponent getTargetComponent()
    {
        return target as TriggerColumnComponent;
    }
}