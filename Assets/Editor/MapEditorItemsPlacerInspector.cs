﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MapEditorItemsPlacerComp))]
public class MapEditorItemsPlacerInspector : UnityEditor.Editor
{
    internal class CharachterData
    {
        internal string displayName;
        internal Mesh mesh;
        internal string prefabName;

        public CharachterData(string displayName, Mesh mesh, string prefabName)
        {
            this.displayName = displayName;
            this.mesh = mesh;
            this.prefabName = prefabName;
        }
    }

    private List<CharachterData> charachterDataList;
    private CharachterData selectedCharachterData;

    private void OnEnable()
    {
        // Init here
        charachterDataList = CreateDataList();
        selectedCharachterData = charachterDataList.First();
    }

    private List<CharachterData> CreateDataList()
    {
        var list = new List<CharachterData>
        {
            new CharachterData("None", null, null),
            new CharachterData("Stone", LoadMeshFromPrefab("CollectibleStone"), "CollectibleStone"),
            new CharachterData("Butterfly", LoadCharachterMesh("ButterflyPrefab"), "ButterflyPrefab"),
            new CharachterData("Special Lilly", LoadMeshFromPrefab("TargetCollectiblePrefab"), "TargetCollectiblePrefab")
        };

        return list;
    }

    private static Mesh LoadCharachterMesh(string prefabname)
    {
        var prefab = LoadPrefab(prefabname);
        var meshRendererr = prefab
            .GetComponentInChildren<SkinnedMeshRenderer>();

        var m = new Mesh();
        meshRendererr.BakeMesh(m);
        return m;
    }

    private static Mesh LoadMeshFromPrefab(string prefabname)
    {
        return LoadPrefab(prefabname)
            .GetComponentInChildren<MeshFilter>()
            .sharedMesh;
    }

    private static GameObject LoadPrefab(string prefabname)
    {
        return (GameObject) Resources.Load("Prefabs/" + prefabname);
    }

    private void OnDisable()
    {
        //TODO : Cleanup here
    }


//    public override void OnInspectorGUI()
//    {
//        DrawDefaultInspector();
//    }

    private void OnSceneGUI()
    {
        DrawSidePanel();
        HandlePlacement();
        SceneView.RepaintAll();
    }

    private void HandlePlacement()
    {
        if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
        {
//            //Debug.Log("Left-Click Down");
        }
        if (Event.current.type == EventType.MouseUp && Event.current.button == 0)
        {
//            //Debug.Log("Left-Click Up");
            if (selectedCharachterData.prefabName != null)
                getTargetComponent().PlaceObjectOnGrid(LoadPrefab(selectedCharachterData.prefabName));
        }

        if (Event.current.type == EventType.MouseDown && Event.current.button == 1)
        {
//            //Debug.Log("Right-Click Down");
        }
        if (Event.current.type == EventType.MouseUp && Event.current.button == 1)
        {
//            //Debug.Log("Right-Click Up");
            getTargetComponent().DeleteObjectOnCurrentPosition();
        }
    }

    private void DrawSidePanel()
    {
        Handles.BeginGUI();
        {
            GUILayout.BeginVertical();
            {
                GUILayout.BeginArea(new Rect(170, 20, 70, 500));
                {
                    GUILayout.Label("Items");
                    foreach (var data in charachterDataList)
                    {
                        if (GUILayout.Button(data.displayName,
                            GUILayout.MinHeight(50), GUILayout.MinWidth(250)))
                        {
                            getTargetComponent().SelectedMesh = data.mesh;
                            selectedCharachterData = data;
                        }
                    }
                }
                GUILayout.EndArea();
            }
            GUILayout.EndVertical();
        }
        Handles.EndGUI();
    }


    /// <summary>
    /// Gets target as inspected component type
    /// </summary>
    /// <returns></returns>
    private MapEditorItemsPlacerComp getTargetComponent()
    {
        return target as MapEditorItemsPlacerComp;
    }
}