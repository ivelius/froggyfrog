﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarAchievmentScript : MonoBehaviour {
	
	public GameObject[] stars;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void ShowAchievedStars(int amountOfStarsAchieved)
	{
		for (var i = 0; i < amountOfStarsAchieved; i++)
			stars[i].SetActive(true);
	}
}
